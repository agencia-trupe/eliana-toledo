-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: previaeliana.mysql.dbaas.com.br
-- Generation Time: 19-Jul-2016 às 16:51
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.22-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `previaeliana`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aromas`
--

CREATE TABLE `aromas` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `aromas`
--

INSERT INTO `aromas` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Todos n&oacute;s adoramos quando chegamos em um lugar e sentimos um aroma, trazendo uma boa energia ao ambiente. Pensando nisso, desenvolvemos uma ess&ecirc;ncia exclusiva com notas de cidra italiana, cramberry, pau-brasil, s&aacute;lvia e &acirc;mbar. &Eacute; uma fragr&acirc;ncia cl&aacute;ssica, sofisticada, elegante e sedutora.</p>\r\n', 'foto_20160609165728.JPG', NULL, '2016-06-09 19:57:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(11, 0, '9_20160614173044.jpg', '2016-06-14 20:30:46', '2016-06-14 20:30:46'),
(12, 0, '10_20160614173053.jpg', '2016-06-14 20:30:55', '2016-06-14 20:30:55'),
(13, 0, '11_20160614173105.jpg', '2016-06-14 20:31:07', '2016-06-14 20:31:07'),
(14, 0, '12_20160614173115.jpg', '2016-06-14 20:31:17', '2016-06-14 20:31:17'),
(15, 0, '13_20160614173130.jpg', '2016-06-14 20:31:32', '2016-06-14 20:31:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `google_maps`, `facebook`, `instagram`, `pinterest`, `created_at`, `updated_at`) VALUES
(1, 'contato@elianatoledo.com.br', '11 2613·6484', '<p>Rua Dr. Sodr&eacute; 122&nbsp;&middot; cj 44&nbsp;&middot; Vila Nova Concei&ccedil;&atilde;o</p>\r\n\r\n<p>04535-110&nbsp;&middot; S&atilde;o Paulo, SP</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.348018648901!2d-46.67604408502163!3d-23.591848584667737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce575716aab959%3A0xa805ff8bc19b260f!2sCondom%C3%ADnio+Edif%C3%ADcio+S%C3%A3o+Luiz+Business+Center+-+R.+Dr.+Sodr%C3%A9%2C+122+-+Vila+Nova+Concei%C3%A7%C3%A3o%2C+S%C3%A3o+Paulo+-+SP%2C+04535-110!5e0!3m2!1spt-BR!2sbr!4v1464206389133" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://www.facebook.com/Eliana-Toledo-Arquitetura-e-Interiores-113206898725524/', 'https://www.instagram.com/elianatoledoarquitetura', 'https://br.pinterest.com/elianatoledoarq', NULL, '2016-06-17 15:20:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `telefone`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'fe', 'fearouche@hotmail.com', '', 'oi', 0, '2016-06-07 01:40:38', '2016-06-07 01:40:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `divisorias`
--

CREATE TABLE `divisorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `divisorias`
--

INSERT INTO `divisorias` (`id`, `ordem`, `titulo`, `imagem`, `descricao`, `created_at`, `updated_at`) VALUES
(2, 0, '.', '1_20160530204621.jpg', '.', '2016-05-30 23:46:27', '2016-05-30 23:46:27'),
(3, 0, '.', '2_20160530204639.jpg', '.', '2016-05-30 23:46:40', '2016-05-30 23:46:40'),
(4, 0, '.', '3_20160530204655.JPG', '.', '2016-05-30 23:47:02', '2016-05-30 23:47:02'),
(5, 0, '.', '4_20160530204713.jpg', '.', '2016-05-30 23:47:14', '2016-05-30 23:47:14'),
(6, 0, '.', '5_20160530204730.JPG', '.', '2016-05-30 23:47:35', '2016-05-30 23:47:35'),
(7, 0, '.', '6_20160530204749.JPG', '.', '2016-05-30 23:47:55', '2016-05-30 23:47:55'),
(8, 0, '.', '7_20160530204805.JPG', '.', '2016-05-30 23:48:07', '2016-05-30 23:48:07'),
(9, 0, '.', '8_20160530204818.JPG', '.', '2016-05-30 23:48:22', '2016-05-30 23:48:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `divisorias_abertura`
--

CREATE TABLE `divisorias_abertura` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `divisorias_abertura`
--

INSERT INTO `divisorias_abertura` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>As divis&oacute;rias s&atilde;o conhecidas tamb&eacute;m como Cobog&oacute;s, esse elemento na arquitetura tr&aacute;s&nbsp; leveza, proporcionando um ambiente especial e &uacute;nico. Sua estrutura vazada possibilita a melhor circula&ccedil;&atilde;o de ar e faz uma brincadeira com a luz natural. Eliana Toledo desenha Cobog&oacute;s exclusivos em diferentes formas, utilizando diferentes materiais e cores, como a laca e madeira.</p>\r\n', 'foto-20160530200019_20160530204422.JPG', NULL, '2016-05-30 23:45:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_05_19_234404_create_banners_table', 1),
('2016_05_19_235231_create_perfil_table', 1),
('2016_05_20_001812_create_publicacoes_table', 1),
('2016_05_20_163501_create_projetos_table', 1),
('2016_05_20_170455_create_aromas_table', 1),
('2016_05_20_171441_create_divisorias_table', 1),
('2016_05_20_172558_create_mobiliario_table', 1),
('2016_05_20_172739_create_mobiliario_abertura_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mobiliario`
--

CREATE TABLE `mobiliario` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `mobiliario`
--

INSERT INTO `mobiliario` (`id`, `ordem`, `titulo`, `imagem`, `descricao`, `created_at`, `updated_at`) VALUES
(115, 0, 'APARADORES', '1_20160617120958.jpg', '', '2016-06-17 15:09:58', '2016-06-17 15:09:58'),
(116, 0, 'BUFFETS', '1_20160617121051.jpg', '', '2016-06-17 15:10:51', '2016-06-17 15:10:51'),
(117, 0, 'CÔMODAS', '1_20160617121139.jpg', '', '2016-06-17 15:11:40', '2016-06-17 15:11:40'),
(118, 0, 'CRIADOS-MUDOS', '1_20160617121218.jpg', '', '2016-06-17 15:12:18', '2016-06-17 15:12:55'),
(119, 0, 'ESCRITÓRIO', '1_20160617121315.JPG', '', '2016-06-17 15:13:16', '2016-06-17 15:13:16'),
(120, 0, 'ESTANTES', '1_20160617121344.jpg', '', '2016-06-17 15:13:44', '2016-06-17 15:13:44'),
(121, 0, 'MESA DE CENTRO', '1_20160617121414.jpg', '', '2016-06-17 15:14:14', '2016-06-17 15:14:14'),
(122, 0, 'MESAS LATERAIS', '1_20160617121449.jpg', '', '2016-06-17 15:14:49', '2016-06-17 15:14:49'),
(123, 0, 'RACKS', '1_20160617121523.jpg', '', '2016-06-17 15:15:23', '2016-06-17 15:15:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mobiliario_abertura`
--

CREATE TABLE `mobiliario_abertura` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `mobiliario_abertura`
--

INSERT INTO `mobiliario_abertura` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Um novo d&eacute;cor implica novos designs.</p>\r\n\r\n<p>Descubra nossas pe&ccedil;as exclusivas, desenvolvidas para suprir a sua necessidade, com design contempor&acirc;neo e funcional.</p>\r\n\r\n<p>Eliana Toledo assina as pe&ccedil;as com uma marcenaria impec&aacute;vel.</p>\r\n', 'foto_20160530200019.JPG', NULL, '2016-05-30 23:00:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mobiliario_imagens`
--

CREATE TABLE `mobiliario_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobiliario_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `mobiliario_imagens`
--

INSERT INTO `mobiliario_imagens` (`id`, `mobiliario_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 115, 2, '2_20160617121019.jpg', '2016-06-17 15:10:20', '2016-06-17 15:10:20'),
(4, 115, 3, '3_20160617121020.jpg', '2016-06-17 15:10:20', '2016-06-17 15:10:20'),
(5, 115, 4, '4_20160617121021.JPG', '2016-06-17 15:10:22', '2016-06-17 15:10:22'),
(6, 116, 0, '2_20160617121059.jpg', '2016-06-17 15:11:00', '2016-06-17 15:11:00'),
(7, 116, 0, '3_20160617121102.jpg', '2016-06-17 15:11:02', '2016-06-17 15:11:02'),
(8, 116, 0, '1_20160617121105.jpg', '2016-06-17 15:11:05', '2016-06-17 15:11:05'),
(9, 115, 1, '1_20160617121120.jpg', '2016-06-17 15:11:20', '2016-06-17 15:11:20'),
(10, 117, 0, '1_20160617121145.jpg', '2016-06-17 15:11:46', '2016-06-17 15:11:46'),
(11, 118, 0, '3_20160617121229.jpg', '2016-06-17 15:12:29', '2016-06-17 15:12:29'),
(12, 118, 0, '1_20160617121229.jpg', '2016-06-17 15:12:29', '2016-06-17 15:12:29'),
(13, 118, 0, '2_20160617121229.jpg', '2016-06-17 15:12:29', '2016-06-17 15:12:29'),
(14, 119, 0, '2_20160617121328.jpg', '2016-06-17 15:13:29', '2016-06-17 15:13:29'),
(15, 119, 0, '1_20160617121330.JPG', '2016-06-17 15:13:31', '2016-06-17 15:13:31'),
(16, 120, 0, '1_20160617121354.jpg', '2016-06-17 15:13:54', '2016-06-17 15:13:54'),
(17, 120, 0, '2_20160617121354.jpg', '2016-06-17 15:13:54', '2016-06-17 15:13:54'),
(18, 121, 0, '1_20160617121430.jpg', '2016-06-17 15:14:30', '2016-06-17 15:14:30'),
(19, 121, 0, '2_20160617121430.jpg', '2016-06-17 15:14:30', '2016-06-17 15:14:30'),
(20, 121, 0, '3_20160617121433.JPG', '2016-06-17 15:14:36', '2016-06-17 15:14:36'),
(21, 122, 0, '1_20160617121500.jpg', '2016-06-17 15:15:00', '2016-06-17 15:15:00'),
(22, 122, 0, '3_20160617121501.jpg', '2016-06-17 15:15:01', '2016-06-17 15:15:01'),
(23, 122, 0, '2_20160617121505.jpg', '2016-06-17 15:15:08', '2016-06-17 15:15:08'),
(24, 123, 0, '1_20160617121531.jpg', '2016-06-17 15:15:32', '2016-06-17 15:15:32'),
(25, 123, 0, '2_20160617121532.jpg', '2016-06-17 15:15:32', '2016-06-17 15:15:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<h2 class="perfil-titulo">UM POUCO DE HIST&Oacute;RIA...</h2>\r\n\r\n<p><strong><em>Era uma vez</em></strong>... uma menina que adorava desenhar casas, pr&eacute;dios, apartamentos, ... imaginava nos seus desenhos, como as pessoas viveriam e trabalhariam nesses locais. Aproveitava panfletos que continham plantas baixas, entregues no lan&ccedil;amento de novos empreendimentos, para criar o mobili&aacute;rio sobre eles e dar asas a sua criatividade.</p>\r\n\r\n<p>A menina cresceu palpitando na decora&ccedil;&atilde;o da sua casa e criando o seu pr&oacute;prio quarto decorado com 11 anos.</p>\r\n\r\n<p>Sentiu cada vez mais, o desejo de usar as suas ideias, para transformar a vida das pessoas, atrav&eacute;s das suas moradias. Naturalmente optou seguir sua carreira na Arquitetura, ingressou na Faculdade de Arquitetura e Urbanismo na Faap e posteriormente se especializou em Design de Interiores. Logo ap&oacute;s montou o seu primeiro escrit&oacute;rio em sociedade.</p>\r\n\r\n<h3 class="perfil-destaque">Hoje, h&aacute; 8 anos, a frente do seu pr&oacute;prio escrit&oacute;rio, Eliana Toledo, utiliza todo seu conhecimento, alinhando criatividade, senso est&eacute;tico, estilo contempor&acirc;neo, com ambientes&nbsp; cleans, funcionais, com harmonia de cores e uma ilumina&ccedil;&atilde;o equilibrada para realizar os sonhos de morar e viver bem.</h3>\r\n\r\n<p>A inspira&ccedil;&atilde;o vem dos mais variados lugares, da inf&acirc;ncia, das viagens ao redor do mundo, do contato com culturas locais, filmes, livros, revistas especializadas e de novas tend&ecirc;ncias, mas principalmente <strong>a inspira&ccedil;&atilde;o vem da est&oacute;ria de cada cliente.</strong></p>\r\n\r\n<p>S&atilde;o 20 anos de carreira com o prazer de ter na sua maior aptid&atilde;o, a sua causa di&aacute;ria, transformando ambientes, sentimentos e sonhos.</p>\r\n', 'img-perfil_20160525195322.png', NULL, '2016-05-30 22:05:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE `projetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `planta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `projetos_categoria_id`, `ordem`, `imagem`, `titulo`, `slug`, `planta`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 3, 0, '1_20160530190841.JPG', 'CHÁCARA KLABIN', 'chacara-klabin', 'chacara-klabin-simone-planta-de-layout_20160530190842.jpg', 'O apartamento tem uma cara jovem, com muita graça em suas cores neutras, sem perder o conforto. A iluminação foi pensada com muita atenção, proporcionando aconchego e sofisticação.', '2016-05-25 19:56:38', '2016-05-30 22:08:43'),
(3, 3, 0, '03_20160602163601.JPG', 'JARDINS', 'jardins', 'jardins_20160602163602.jpg', 'Nesse lindo apartamento localizado nos Jardins, a arquiteta optou por cores em tom de cinza, branco e madeira natural. A pedido de seus moradores, um jovem casal, que adoram receber amigos para cozinhar, foi feito um Passa Prato, com um trabalho de marcenaria impecável.', '2016-05-30 22:15:29', '2016-06-02 19:36:03'),
(4, 3, 0, '10_20160530191704.JPG', 'PERDIZES', 'perdizes', 'perdizes-planta-layout_20160602160607.jpg', 'O projeto desse espaçoso duplex valoriza a integração dos espaços, como o living, jantar e o Home Theater, ocupando todos um mesmo espaço. As cores dos objetos decorativos são um dos destaques, trazendo muita personalidade ao projeto com peças garimpadas pelo mundo, traço de sua proprietária que adora viajar.', '2016-05-30 22:17:16', '2016-06-02 19:10:49'),
(5, 3, 0, '5_20160614172648.JPG', 'VILA NOVA', 'vila-nova', 'vila-nova-planta-de-layout_20160602170111.jpg', 'Projeto de design de interiores de um edifício localizado no bairro paulistano Vila Nova Conceição, ele foi totalmente repaginado. O hall de entrada ganhou novos ares com um lindo revestimento em madeira e decoração clean. O salão de festas tem uma identidade contemporânea e charmosa integrado a um espaço gourmet, muito apreciado pelos moradores do condomínio.', '2016-05-30 22:19:54', '2016-06-14 20:26:49'),
(6, 3, 0, '1_20160530192605.JPG', 'VILA MASCOTE', 'vila-mascote', 'vila-mascote-planta-layout_20160602165546.jpg', 'Nesse gracioso apartamento, foi aproveitado cada centímetro para valorizar o imóvel. Da escolha de seu piso em madeira natural aos revestimentos em marcenaria personalizada, a arquiteta faz uso de espelhos para gerar amplitude ao ambiente, utilizando uma iluminação na medida certa.', '2016-05-30 22:26:06', '2016-06-02 19:55:48'),
(7, 3, 0, '1_20160530192818.jpg', 'FABIA', 'fabia', 'fabia-paula-e-paulo-layout_20160530192818.jpg', 'O apartamento passou por uma grande reforma de atualização. Seus moradores adoram receber seus amigos, pensando nisso foi projetado um ambiente na varanda que abriga um espaço gourmet com uma chopeira, lugar de destaque desse charmoso apartamento.', '2016-05-30 22:28:19', '2016-06-02 19:23:03'),
(8, 3, 0, '1_20160530193416.JPG', 'AICAS', 'aicas', '', '.', '2016-05-30 22:34:18', '2016-05-31 15:39:30'),
(9, 1, 0, '1-9_20160614173618.jpg', 'CASA GOURMET 2008', 'casa-gourmet-2008', '1-1_20160530204132.jpg', '.', '2016-05-30 23:41:33', '2016-06-14 20:36:18'),
(10, 1, 0, '2_20160530204407.jpg', 'EUROVILLE 2015', 'euroville-2015', '1_20160530204409.JPG', '.', '2016-05-30 23:44:11', '2016-05-31 15:40:30'),
(11, 3, 0, '1_20160530205249.jpg', 'MOEMA', 'moema', 'moema-planta-de-layout_20160530205250.jpg', 'Integração, é a palavra chave nesse projeto, algumas paredes foram desfeitas, tais como: sala e cozinha. Foi aproveitado cada centímetro de todos os ambientes para acolher de melhor forma os seus moradores, que são um jovem casal com seu filho. Nesse projeto, Eliana partiu do zero para escolher cada detalhe.', '2016-05-30 23:52:50', '2016-05-31 15:39:18'),
(12, 2, 0, '1_20160531154608.JPG', 'AMBULATÓRIO MEDIC STAR - SHOPPING MORUMBI', 'ambulatorio-medic-star-shopping-morumbi', 'layout-ambulatorio-morumbi_20160531154609.jpg', 'O projeto desses ambulatórios proporcionam aos seus pacientes um ambiente agradável com muito conforto, bem-estar e segurança. O trabalho de marcenaria se faz presente em todo o projeto, desenhado por Eliana Toledo.', '2016-05-31 18:46:09', '2016-05-31 18:46:09'),
(13, 2, 0, '1_20160531154659.JPG', 'AMBULATÓRIO MEDIC STAR - SHOPPING SP MARKET', 'ambulatorio-medic-star-shopping-sp-market', 'layout-ambulatorio-sp-market_20160531154701.JPG', 'O projeto desses ambulatórios proporcionam aos seus pacientes um ambiente agradável com muito conforto, bem-estar e segurança. O trabalho de marcenaria se faz presente em todo o projeto, desenhado por Eliana Toledo.', '2016-05-31 18:47:02', '2016-05-31 18:47:02'),
(14, 2, 0, '4_20160614173455.jpg', 'COMPACTA PRINT', 'compacta-print', 'layout-compact-print_20160614173422.JPG', '.', '2016-05-31 18:49:21', '2016-06-14 20:34:55'),
(15, 2, 0, '1_20160531155017.jpg', 'CONSULTÓRIO ODONTOLÓGICO', 'consultorio-odontologico', 'planta-odontologia_20160531155017.jpg', 'Esse projeto de consultório odontológico conta com um layout limpo, trazendo sensação de bem-estar e conforto para os seus pacientes. Ele possui um fluxo de circulação que proporciona eficiência para todos os procedimentos feitos pelo profissional, com um trabalho de marcenaria funcional e moderno.', '2016-05-31 18:50:18', '2016-05-31 18:50:18'),
(16, 2, 0, '1-2_20160531155135.JPG', 'CONSULTÓRIO PSICOLOGIA', 'consultorio-psicologia', 'layout-consultorio-psicologia_20160531155135.JPG', 'Esse consultório de psicologia, acomoda de maneira inteligente e aconchegante suas atividades. O ambiente foi projetado para que seus pacientes sentissem conforto e confiança, com a sensação de estar em suas próprias casas.', '2016-05-31 18:51:36', '2016-05-31 18:51:36'),
(17, 2, 0, '1_20160531155306.JPG', 'ESCRITÓRIO ELIANA', 'escritorio-eliana', '', 'O ambiente de trabalho é extremamente importante e merece atenção especial, pois além de produtivo ele deve se tornar mais atrativo e agradável. O projeto do escritório ELIANA TOLEDO tem uma proposta moderna e dinâmica, integrando todo o ambiente, havendo uma interação entre seus profissionais.', '2016-05-31 18:53:08', '2016-06-14 20:27:31'),
(18, 2, 0, '1-1_20160531155517.jpg', 'IMOBILIÁRIA', 'imobiliaria', 'layout-imobiliaria-pav-superior_20160531155518.JPG', 'O desafio deste projeto foi integrar qualidade e modernidade. A sala de reuniões conta com privacidade, climatização artificial e uso de plantas preservadas, dando um ar descontraído. O layout da imobiliária seguiu as necessidades para o seu funcionamento, criando um ambiente atrativo para seus clientes.', '2016-05-31 18:55:18', '2016-05-31 18:55:18'),
(19, 2, 0, '1_20160531155637.jpg', 'LOJA BAGNARE', 'loja-bagnare', 'planta-loja-bagnare_20160531155637.JPG', 'O projeto dessa loja de cosméticos situada no shopping Frei Caneca teve como desafio aproveitar cada centímetro do seu espaço reduzido. Os produtos vendidos ficam à mostra para que seus clientes possam visualizar e experimentar sem restrições. A marcenaria foi projetada na cor branca, dando destaque aos produtos da loja. ', '2016-05-31 18:56:37', '2016-05-31 18:56:37'),
(20, 3, 0, '8_20160602164405.JPG', 'PACAEMBU', 'pacaembu', 'pacaembu-planta-home-thater_20160531165929.jpg', 'Nessa antiga casa localizada no Pacaembu, sua estrutura externa foi totalmente refeita e suas áreas internas foram repaginadas. Toda parte de revestimento e infraestrutura foi substituída por nova. O espaço para o Home Theater é o lugar preferido do casal que mora nessa linda casa.', '2016-05-31 19:59:30', '2016-06-02 19:44:10'),
(21, 3, 0, '8_20160602155948.jpg', 'ALTO DA LAPA', 'alto-da-lapa', 'alto-da-lapa-02-layout_20160602155948.jpg', 'Este apartamento recebeu um projeto de decoração de interiores totalmente repaginado. A proposta foi integrar os três espaços: jantar, living e home theater, dando a sensação de amplitude nas áreas sociais.', '2016-06-02 18:59:50', '2016-06-02 18:59:50'),
(22, 3, 0, '1_20160602160252.jpg', 'CARLOS WEBER', 'carlos-weber', '', 'Apartamento em estilo leve, tons suaves e iluminação aconchegante. Em função da metragem, o mesmo ambiente ganha uso diversos, como o living que abriga um espaço para o Home Office.', '2016-06-02 19:02:52', '2016-06-02 19:02:52'),
(23, 3, 0, '1_20160602162121.jpg', 'EUCALIPTOS', 'eucaliptos', '', 'Projeto de decoração de um charmoso duplex em Moema, ele foi pensado para um jovem solteiro que procurava conforto sem dispensar a elegância. Suas cores são neutras e o trabalho de marcenaria se faz presente em todo o imóvel.', '2016-06-02 19:21:21', '2016-06-14 20:25:06'),
(24, 3, 0, '1_20160602162514.jpg', 'HÍPICA', 'hipica', 'hipica-layout_20160602162514.jpg', 'A decoração desse projeto é contemporânea, com móveis de design e peças assinadas. Esse projeto mescla o uso de madeiras nobres, laca branca, espelhos e revestimento em papel de parede, proporcionando um resultado incrível.', '2016-06-02 19:25:15', '2016-06-02 19:25:15'),
(25, 3, 0, '1_20160602162714.jpg', 'ITAIM BIBI', 'itaim-bibi', 'itaim-bibi-layout_20160602162714.jpg', 'Bem estar, conforto e sofisticação, são as premissas para compor o decor. Em tons neutros e beges, dando um ar clean e natural, como solicitado pelos moradores. A marcenaria foi totalmente personalizada, com painéis e estantes que ganharam iluminação embutida, proporcionando aconchego aos ambientes.', '2016-06-02 19:27:15', '2016-06-02 19:27:15'),
(26, 3, 0, '1_20160602162915.jpg', 'LEOPOLDINA', 'leopoldina', '', 'Nesse agradável imóvel, faz-se presente a laca branca, espelhos e iluminação aconchegante. A escolha dos estofados em cores neutras e florais proporcionam leveza e delicadeza nesse charmoso ambiente.', '2016-06-02 19:29:15', '2016-06-02 19:29:15'),
(27, 3, 0, '4_20160602164719.jpg', 'PANAMERICANA', 'panamericana', '', 'A leveza dos ambientes é o que mais se destaca neste projeto. Toda parte de móveis foi comprada, assim como tapetes e cortinas. A pedido dos moradores, o quarto de sua filha, foi feito em tons de lilás e branco, trazendo identidade e aconchego.', '2016-06-02 19:47:19', '2016-06-02 19:47:19'),
(28, 3, 0, '1_20160602170447.jpg', 'VILA OLÍMPIA', 'vila-olimpia', 'vila-olimpia-planta-de-layout_20160602170447.jpg', 'Equilíbrio e leveza, são conceitos que definem o projeto desse charmoso apartamento elaborado para uma mulher, sua proprietária. Nele foram utilizados o uso de pedras, laca branca, tecidos nobres e cores neutras, com um toque feminino.', '2016-06-02 20:04:48', '2016-06-02 20:04:48'),
(29, 3, 0, '1_20160602170709.jpg', 'VILA ROMANA', 'vila-romana', 'vila-romana-layout-model_20160602170709.jpg', 'No amplo e requintado apartamento paulistano, o terraço de dimensões generosas, é um agradável atrativo do imóvel, onde foi possível acomodar um estar e um gourmet. A combinação equilibrada dos elementos, transmitem uma sensação de bem estar que seus moradores apreciam satisfeitos. ', '2016-06-02 20:07:09', '2016-06-02 20:07:09'),
(30, 3, 0, '1_20160602170932.jpg', 'VILLA LOBOS', 'villa-lobos', '', 'Felizes habitantes hoje deste aconchegante imóvel de linhas contemporâneas, esse foi o fio condutor desse projeto. Ele foi pensado para um jovem casal e suas duas filhas. No living teve um trabalho de marcenaria arrojado, com divisórias vazadas e revestimento em madeira que deixou o ambiente repleto de elegância.', '2016-06-02 20:09:33', '2016-06-02 20:09:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_categorias`
--

CREATE TABLE `projetos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_categorias`
--

INSERT INTO `projetos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'MOSTRAS', 'mostras', '2016-05-25 19:53:42', '2016-05-25 19:53:42'),
(2, 0, 'COMERCIAL', 'comercial', '2016-05-25 19:53:47', '2016-05-25 19:53:47'),
(3, 0, 'RESIDENCIAL', 'residencial', '2016-05-25 19:53:53', '2016-05-25 19:53:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_imagens`
--

CREATE TABLE `projetos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `projeto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(78, 9, 0, '1-26_20160530204154.JPG', '2016-05-30 23:41:55', '2016-05-30 23:41:55'),
(79, 9, 0, '1-3_20160530204155.jpg', '2016-05-30 23:41:56', '2016-05-30 23:41:56'),
(80, 9, 0, '1-4_20160530204155.jpg', '2016-05-30 23:41:56', '2016-05-30 23:41:56'),
(81, 9, 0, '1-5_20160530204155.jpg', '2016-05-30 23:41:56', '2016-05-30 23:41:56'),
(82, 9, 0, '1-6_20160530204156.jpg', '2016-05-30 23:41:56', '2016-05-30 23:41:56'),
(83, 9, 0, '1-9_20160530204156.jpg', '2016-05-30 23:41:56', '2016-05-30 23:41:56'),
(84, 9, 0, '1-10_20160530204156.jpg', '2016-05-30 23:41:56', '2016-05-30 23:41:56'),
(85, 9, 0, '1-11_20160530204157.jpg', '2016-05-30 23:41:57', '2016-05-30 23:41:57'),
(86, 9, 0, '1-13_20160530204157.jpg', '2016-05-30 23:41:58', '2016-05-30 23:41:58'),
(87, 9, 0, '1-12_20160530204157.jpg', '2016-05-30 23:41:58', '2016-05-30 23:41:58'),
(88, 9, 0, '1-14_20160530204158.jpg', '2016-05-30 23:41:58', '2016-05-30 23:41:58'),
(89, 9, 0, '1-15_20160530204158.jpg', '2016-05-30 23:41:58', '2016-05-30 23:41:58'),
(90, 9, 0, '1-16_20160530204158.jpg', '2016-05-30 23:41:58', '2016-05-30 23:41:58'),
(91, 9, 0, '1-17_20160530204158.jpg', '2016-05-30 23:41:59', '2016-05-30 23:41:59'),
(92, 9, 0, '1-18_20160530204159.jpg', '2016-05-30 23:41:59', '2016-05-30 23:41:59'),
(93, 9, 0, '1-19_20160530204159.jpg', '2016-05-30 23:42:00', '2016-05-30 23:42:00'),
(94, 9, 0, '1-24_20160530204159.JPG', '2016-05-30 23:42:00', '2016-05-30 23:42:00'),
(95, 9, 0, '1-20_20160530204159.jpg', '2016-05-30 23:42:00', '2016-05-30 23:42:00'),
(96, 10, 0, '4_20160530204446.JPG', '2016-05-30 23:44:49', '2016-05-30 23:44:49'),
(97, 10, 0, '6_20160530204446.JPG', '2016-05-30 23:44:51', '2016-05-30 23:44:51'),
(98, 10, 0, '3_20160530204446.JPG', '2016-05-30 23:44:56', '2016-05-30 23:44:56'),
(99, 10, 0, '5_20160530204450.JPG', '2016-05-30 23:44:57', '2016-05-30 23:44:57'),
(145, 12, 0, '2_20160531154623.JPG', '2016-05-31 18:46:24', '2016-05-31 18:46:24'),
(146, 13, 0, '2_20160531154716.JPG', '2016-05-31 18:47:17', '2016-05-31 18:47:17'),
(147, 13, 0, '1_20160531154742.JPG', '2016-05-31 18:47:44', '2016-05-31 18:47:44'),
(148, 12, 0, '1_20160531154759.JPG', '2016-05-31 18:48:00', '2016-05-31 18:48:00'),
(149, 15, 0, '2_20160531155040.jpg', '2016-05-31 18:50:41', '2016-05-31 18:50:41'),
(150, 15, 0, '3_20160531155040.jpg', '2016-05-31 18:50:41', '2016-05-31 18:50:41'),
(151, 15, 0, '1_20160531155039.jpg', '2016-05-31 18:50:42', '2016-05-31 18:50:42'),
(152, 16, 0, '1-4_20160531155155.JPG', '2016-05-31 18:51:58', '2016-05-31 18:51:58'),
(153, 16, 0, '1-2_20160531155154.JPG', '2016-05-31 18:51:58', '2016-05-31 18:51:58'),
(154, 16, 0, '1-3_20160531155155.JPG', '2016-05-31 18:52:03', '2016-05-31 18:52:03'),
(155, 16, 0, '1-5_20160531155156.JPG', '2016-05-31 18:52:03', '2016-05-31 18:52:03'),
(156, 17, 0, '2_20160531155342.JPG', '2016-05-31 18:53:48', '2016-05-31 18:53:48'),
(157, 17, 0, '1_20160531155342.JPG', '2016-05-31 18:53:48', '2016-05-31 18:53:48'),
(158, 17, 0, '3_20160531155343.JPG', '2016-05-31 18:53:55', '2016-05-31 18:53:55'),
(159, 17, 0, '5_20160531155345.JPG', '2016-05-31 18:53:58', '2016-05-31 18:53:58'),
(160, 17, 0, '6_20160531155353.JPG', '2016-05-31 18:54:00', '2016-05-31 18:54:00'),
(161, 17, 0, '4_20160531155359.JPG', '2016-05-31 18:54:04', '2016-05-31 18:54:04'),
(162, 17, 0, '8_20160531155403.JPG', '2016-05-31 18:54:09', '2016-05-31 18:54:09'),
(163, 17, 0, '7_20160531155402.JPG', '2016-05-31 18:54:13', '2016-05-31 18:54:13'),
(164, 17, 0, '10_20160531155407.JPG', '2016-05-31 18:54:15', '2016-05-31 18:54:15'),
(165, 17, 0, '11_20160531155410.JPG', '2016-05-31 18:54:21', '2016-05-31 18:54:21'),
(166, 17, 0, '9_20160531155409.jpg', '2016-05-31 18:54:26', '2016-05-31 18:54:26'),
(167, 18, 0, '1-1_20160531155531.jpg', '2016-05-31 18:55:35', '2016-05-31 18:55:35'),
(168, 18, 0, '1-5_20160531155535.jpg', '2016-05-31 18:55:36', '2016-05-31 18:55:36'),
(169, 18, 0, '1-6_20160531155536.jpg', '2016-05-31 18:55:37', '2016-05-31 18:55:37'),
(170, 18, 0, '1-8_20160531155536.jpg', '2016-05-31 18:55:37', '2016-05-31 18:55:37'),
(171, 18, 0, 'layout-imobiliaria-pav-inferior_20160531155537.JPG', '2016-05-31 18:55:38', '2016-05-31 18:55:38'),
(172, 18, 0, '1-7_20160531155539.jpg', '2016-05-31 18:55:43', '2016-05-31 18:55:43'),
(173, 18, 0, '1-2_20160531155538.jpg', '2016-05-31 18:55:43', '2016-05-31 18:55:43'),
(174, 19, 0, '1_20160531155657.jpg', '2016-05-31 18:56:58', '2016-05-31 18:56:58'),
(175, 19, 0, '2_20160531155657.jpg', '2016-05-31 18:56:58', '2016-05-31 18:56:58'),
(176, 19, 0, '3_20160531155658.jpg', '2016-05-31 18:56:59', '2016-05-31 18:56:59'),
(196, 21, 0, '01_20160602160054.jpg', '2016-06-02 19:00:55', '2016-06-02 19:00:55'),
(197, 21, 0, '02_20160602160055.jpg', '2016-06-02 19:00:55', '2016-06-02 19:00:55'),
(198, 21, 0, '03_20160602160055.jpg', '2016-06-02 19:00:55', '2016-06-02 19:00:55'),
(199, 21, 0, '04_20160602160055.jpg', '2016-06-02 19:00:56', '2016-06-02 19:00:56'),
(200, 21, 0, '05_20160602160056.jpg', '2016-06-02 19:00:56', '2016-06-02 19:00:56'),
(201, 21, 0, '06_20160602160056.jpg', '2016-06-02 19:00:56', '2016-06-02 19:00:56'),
(202, 21, 0, '07_20160602160056.jpg', '2016-06-02 19:00:57', '2016-06-02 19:00:57'),
(203, 21, 0, '08_20160602160057.jpg', '2016-06-02 19:00:57', '2016-06-02 19:00:57'),
(204, 21, 0, '09_20160602160057.jpg', '2016-06-02 19:00:58', '2016-06-02 19:00:58'),
(205, 21, 0, '10_20160602160057.jpg', '2016-06-02 19:00:58', '2016-06-02 19:00:58'),
(206, 21, 0, '11_20160602160058.jpg', '2016-06-02 19:00:58', '2016-06-02 19:00:58'),
(207, 21, 0, '12_20160602160058.jpg', '2016-06-02 19:00:58', '2016-06-02 19:00:58'),
(208, 21, 0, '13_20160602160058.jpg', '2016-06-02 19:00:58', '2016-06-02 19:00:58'),
(209, 21, 0, '14_20160602160059.jpg', '2016-06-02 19:00:59', '2016-06-02 19:00:59'),
(210, 21, 0, '16_20160602160059.jpg', '2016-06-02 19:01:00', '2016-06-02 19:01:00'),
(211, 21, 0, '15_20160602160059.jpg', '2016-06-02 19:01:00', '2016-06-02 19:01:00'),
(212, 21, 0, '17_20160602160100.jpg', '2016-06-02 19:01:00', '2016-06-02 19:01:00'),
(213, 21, 0, '18_20160602160100.jpg', '2016-06-02 19:01:01', '2016-06-02 19:01:01'),
(214, 21, 0, '19_20160602160101.jpg', '2016-06-02 19:01:01', '2016-06-02 19:01:01'),
(215, 21, 0, '20_20160602160101.jpg', '2016-06-02 19:01:01', '2016-06-02 19:01:01'),
(216, 21, 0, '21_20160602160101.jpg', '2016-06-02 19:01:02', '2016-06-02 19:01:02'),
(217, 22, 0, '1_20160602160306.jpg', '2016-06-02 19:03:06', '2016-06-02 19:03:06'),
(218, 22, 0, '2_20160602160307.jpg', '2016-06-02 19:03:08', '2016-06-02 19:03:08'),
(219, 22, 0, '3_20160602160308.jpg', '2016-06-02 19:03:08', '2016-06-02 19:03:08'),
(220, 22, 0, '4_20160602160308.jpg', '2016-06-02 19:03:09', '2016-06-02 19:03:09'),
(221, 22, 0, '5_20160602160309.jpg', '2016-06-02 19:03:10', '2016-06-02 19:03:10'),
(222, 22, 0, '7_20160602160309.jpg', '2016-06-02 19:03:10', '2016-06-02 19:03:10'),
(223, 22, 0, '8_20160602160309.jpg', '2016-06-02 19:03:10', '2016-06-02 19:03:10'),
(224, 22, 0, '6_20160602160309.jpg', '2016-06-02 19:03:10', '2016-06-02 19:03:10'),
(245, 1, 5, '5_20160602161635.JPG', '2016-06-02 19:16:41', '2016-06-02 19:16:41'),
(246, 1, 4, '4_20160602161635.JPG', '2016-06-02 19:16:42', '2016-06-02 19:16:42'),
(247, 1, 6, '6_20160602161638.JPG', '2016-06-02 19:16:45', '2016-06-02 19:16:45'),
(248, 1, 3, '3_20160602161643.JPG', '2016-06-02 19:16:50', '2016-06-02 19:16:50'),
(249, 1, 8, '8_20160602161647.JPG', '2016-06-02 19:16:54', '2016-06-02 19:16:54'),
(250, 1, 7, '7_20160602161648.JPG', '2016-06-02 19:16:55', '2016-06-02 19:16:55'),
(251, 1, 9, '9_20160602161652.JPG', '2016-06-02 19:16:58', '2016-06-02 19:16:58'),
(252, 1, 10, '10_20160602161656.JPG', '2016-06-02 19:17:02', '2016-06-02 19:17:02'),
(253, 1, 1, '1_20160602161658.JPG', '2016-06-02 19:17:05', '2016-06-02 19:17:05'),
(254, 1, 2, '2_20160602161700.jpg', '2016-06-02 19:17:07', '2016-06-02 19:17:07'),
(255, 23, 0, '02_20160602162159.jpg', '2016-06-02 19:22:01', '2016-06-02 19:22:01'),
(256, 23, 0, '03_20160602162159.jpg', '2016-06-02 19:22:01', '2016-06-02 19:22:01'),
(257, 23, 0, '01_20160602162159.jpg', '2016-06-02 19:22:01', '2016-06-02 19:22:01'),
(258, 23, 0, '05_20160602162201.jpg', '2016-06-02 19:22:04', '2016-06-02 19:22:04'),
(259, 23, 0, '06_20160602162203.jpg', '2016-06-02 19:22:05', '2016-06-02 19:22:05'),
(260, 23, 0, '07_20160602162203.jpg', '2016-06-02 19:22:06', '2016-06-02 19:22:06'),
(261, 23, 0, '09_20160602162204.jpg', '2016-06-02 19:22:08', '2016-06-02 19:22:08'),
(262, 23, 0, '04_20160602162205.jpg', '2016-06-02 19:22:08', '2016-06-02 19:22:08'),
(263, 23, 0, '08_20160602162204.jpg', '2016-06-02 19:22:08', '2016-06-02 19:22:08'),
(264, 23, 0, '10_20160602162206.jpg', '2016-06-02 19:22:09', '2016-06-02 19:22:09'),
(265, 23, 0, '12_20160602162209.jpg', '2016-06-02 19:22:11', '2016-06-02 19:22:11'),
(266, 23, 0, '13_20160602162210.jpg', '2016-06-02 19:22:12', '2016-06-02 19:22:12'),
(267, 23, 0, '11_20160602162208.jpg', '2016-06-02 19:22:14', '2016-06-02 19:22:14'),
(268, 23, 0, '15_20160602162211.jpg', '2016-06-02 19:22:16', '2016-06-02 19:22:16'),
(269, 23, 0, '14_20160602162211.jpg', '2016-06-02 19:22:17', '2016-06-02 19:22:17'),
(270, 7, 0, '1_20160602162327.jpg', '2016-06-02 19:23:27', '2016-06-02 19:23:27'),
(271, 7, 0, '2_20160602162327.jpg', '2016-06-02 19:23:28', '2016-06-02 19:23:28'),
(272, 7, 0, '3_20160602162328.jpg', '2016-06-02 19:23:28', '2016-06-02 19:23:28'),
(273, 7, 0, '4_20160602162328.jpg', '2016-06-02 19:23:28', '2016-06-02 19:23:28'),
(274, 7, 0, '5_20160602162329.jpg', '2016-06-02 19:23:29', '2016-06-02 19:23:29'),
(275, 7, 0, '6_20160602162329.jpg', '2016-06-02 19:23:30', '2016-06-02 19:23:30'),
(276, 7, 0, '8_20160602162330.jpg', '2016-06-02 19:23:30', '2016-06-02 19:23:30'),
(277, 7, 0, '9_20160602162330.jpg', '2016-06-02 19:23:30', '2016-06-02 19:23:30'),
(278, 7, 0, '7_20160602162330.jpg', '2016-06-02 19:23:30', '2016-06-02 19:23:30'),
(279, 24, 0, '02_20160602162547.jpg', '2016-06-02 19:25:48', '2016-06-02 19:25:48'),
(280, 24, 0, '01_20160602162547.jpg', '2016-06-02 19:25:48', '2016-06-02 19:25:48'),
(281, 24, 0, '05_20160602162548.jpg', '2016-06-02 19:25:48', '2016-06-02 19:25:48'),
(282, 24, 0, '04_20160602162548.jpg', '2016-06-02 19:25:49', '2016-06-02 19:25:49'),
(283, 24, 0, '03_20160602162548.jpg', '2016-06-02 19:25:49', '2016-06-02 19:25:49'),
(284, 24, 0, '06_20160602162549.jpg', '2016-06-02 19:25:49', '2016-06-02 19:25:49'),
(285, 24, 0, '10_20160602162550.jpg', '2016-06-02 19:25:50', '2016-06-02 19:25:50'),
(286, 24, 0, '09_20160602162549.jpg', '2016-06-02 19:25:50', '2016-06-02 19:25:50'),
(287, 24, 0, '07_20160602162549.jpg', '2016-06-02 19:25:50', '2016-06-02 19:25:50'),
(288, 24, 0, '11_20160602162550.jpg', '2016-06-02 19:25:50', '2016-06-02 19:25:50'),
(289, 24, 0, '13_20160602162550.jpg', '2016-06-02 19:25:50', '2016-06-02 19:25:50'),
(290, 24, 0, '12_20160602162550.jpg', '2016-06-02 19:25:51', '2016-06-02 19:25:51'),
(291, 25, 0, '01_20160602162756.jpg', '2016-06-02 19:27:57', '2016-06-02 19:27:57'),
(292, 25, 0, '02_20160602162756.jpg', '2016-06-02 19:27:57', '2016-06-02 19:27:57'),
(293, 25, 0, '03_20160602162757.jpg', '2016-06-02 19:27:57', '2016-06-02 19:27:57'),
(294, 25, 0, '04_20160602162757.jpg', '2016-06-02 19:27:58', '2016-06-02 19:27:58'),
(295, 25, 0, '05_20160602162757.jpg', '2016-06-02 19:27:58', '2016-06-02 19:27:58'),
(296, 25, 0, '06_20160602162758.jpg', '2016-06-02 19:27:58', '2016-06-02 19:27:58'),
(297, 25, 0, '07_20160602162800.jpg', '2016-06-02 19:28:00', '2016-06-02 19:28:00'),
(298, 25, 0, '08_20160602162800.jpg', '2016-06-02 19:28:00', '2016-06-02 19:28:00'),
(299, 25, 0, '10_20160602162800.jpg', '2016-06-02 19:28:01', '2016-06-02 19:28:01'),
(300, 25, 0, '09_20160602162800.jpg', '2016-06-02 19:28:01', '2016-06-02 19:28:01'),
(301, 25, 0, '11_20160602162800.jpg', '2016-06-02 19:28:01', '2016-06-02 19:28:01'),
(302, 25, 0, '12_20160602162801.jpg', '2016-06-02 19:28:01', '2016-06-02 19:28:01'),
(303, 25, 0, '13_20160602162803.jpg', '2016-06-02 19:28:03', '2016-06-02 19:28:03'),
(304, 25, 0, '14_20160602162803.jpg', '2016-06-02 19:28:03', '2016-06-02 19:28:03'),
(305, 25, 0, '15_20160602162803.jpg', '2016-06-02 19:28:04', '2016-06-02 19:28:04'),
(306, 25, 0, '16_20160602162803.jpg', '2016-06-02 19:28:04', '2016-06-02 19:28:04'),
(307, 25, 0, '18_20160602162803.JPG', '2016-06-02 19:28:04', '2016-06-02 19:28:04'),
(308, 25, 0, '17_20160602162805.JPG', '2016-06-02 19:28:08', '2016-06-02 19:28:08'),
(309, 25, 0, '19_20160602162808.JPG', '2016-06-02 19:28:11', '2016-06-02 19:28:11'),
(310, 26, 0, '1_20160602162931.jpg', '2016-06-02 19:29:31', '2016-06-02 19:29:31'),
(311, 26, 0, '3_20160602162932.jpg', '2016-06-02 19:29:33', '2016-06-02 19:29:33'),
(312, 26, 0, '2_20160602162934.jpg', '2016-06-02 19:29:34', '2016-06-02 19:29:34'),
(313, 26, 0, '4_20160602162934.jpg', '2016-06-02 19:29:34', '2016-06-02 19:29:34'),
(314, 3, 0, '04_20160602163350.JPG', '2016-06-02 19:33:59', '2016-06-02 19:33:59'),
(315, 3, 0, '06_20160602163353.JPG', '2016-06-02 19:34:01', '2016-06-02 19:34:01'),
(316, 3, 0, '01_20160602163351.JPG', '2016-06-02 19:34:12', '2016-06-02 19:34:12'),
(317, 3, 0, '02_20160602163357.JPG', '2016-06-02 19:34:20', '2016-06-02 19:34:20'),
(318, 3, 0, '05_20160602163400.JPG', '2016-06-02 19:34:22', '2016-06-02 19:34:22'),
(319, 3, 0, '03_20160602163356.JPG', '2016-06-02 19:34:23', '2016-06-02 19:34:23'),
(320, 3, 0, '07_20160602163407.JPG', '2016-06-02 19:34:28', '2016-06-02 19:34:28'),
(321, 3, 0, '08_20160602163409.JPG', '2016-06-02 19:34:31', '2016-06-02 19:34:31'),
(322, 3, 0, '09_20160602163419.JPG', '2016-06-02 19:34:36', '2016-06-02 19:34:36'),
(323, 3, 0, '14_20160602163437.JPG', '2016-06-02 19:34:42', '2016-06-02 19:34:42'),
(324, 3, 0, '15_20160602163440.JPG', '2016-06-02 19:34:46', '2016-06-02 19:34:46'),
(325, 3, 0, '10_20160602163430.JPG', '2016-06-02 19:34:47', '2016-06-02 19:34:47'),
(326, 3, 0, '11_20160602163437.JPG', '2016-06-02 19:34:47', '2016-06-02 19:34:47'),
(327, 3, 0, '13_20160602163440.JPG', '2016-06-02 19:34:52', '2016-06-02 19:34:52'),
(328, 3, 0, '12_20160602163438.JPG', '2016-06-02 19:34:54', '2016-06-02 19:34:54'),
(329, 3, 0, '16_20160602163447.JPG', '2016-06-02 19:34:57', '2016-06-02 19:34:57'),
(330, 11, 0, '01_20160602163804.jpg', '2016-06-02 19:38:04', '2016-06-02 19:38:04'),
(331, 11, 0, '02_20160602163804.jpg', '2016-06-02 19:38:05', '2016-06-02 19:38:05'),
(332, 11, 0, '04_20160602163804.jpg', '2016-06-02 19:38:05', '2016-06-02 19:38:05'),
(333, 11, 0, '06_20160602163805.jpg', '2016-06-02 19:38:05', '2016-06-02 19:38:05'),
(334, 11, 0, '03_20160602163806.jpg', '2016-06-02 19:38:07', '2016-06-02 19:38:07'),
(335, 11, 0, '05_20160602163806.jpg', '2016-06-02 19:38:07', '2016-06-02 19:38:07'),
(336, 11, 0, '07_20160602163806.jpg', '2016-06-02 19:38:07', '2016-06-02 19:38:07'),
(337, 11, 0, '09_20160602163807.jpg', '2016-06-02 19:38:07', '2016-06-02 19:38:07'),
(338, 11, 0, '10_20160602163808.jpg', '2016-06-02 19:38:08', '2016-06-02 19:38:08'),
(339, 11, 0, '11_20160602163808.jpg', '2016-06-02 19:38:09', '2016-06-02 19:38:09'),
(340, 11, 0, '12_20160602163808.jpg', '2016-06-02 19:38:09', '2016-06-02 19:38:09'),
(341, 11, 0, '08_20160602163809.jpg', '2016-06-02 19:38:09', '2016-06-02 19:38:09'),
(342, 11, 0, '14_20160602163809.jpg', '2016-06-02 19:38:10', '2016-06-02 19:38:10'),
(343, 11, 0, '15_20160602163810.jpg', '2016-06-02 19:38:10', '2016-06-02 19:38:10'),
(344, 11, 0, '16_20160602163811.jpg', '2016-06-02 19:38:11', '2016-06-02 19:38:11'),
(345, 11, 0, '17_20160602163811.jpg', '2016-06-02 19:38:11', '2016-06-02 19:38:11'),
(346, 11, 0, '13_20160602163811.jpg', '2016-06-02 19:38:12', '2016-06-02 19:38:12'),
(347, 11, 0, '19_20160602163811.jpg', '2016-06-02 19:38:12', '2016-06-02 19:38:12'),
(348, 11, 0, '20_20160602163811.jpg', '2016-06-02 19:38:12', '2016-06-02 19:38:12'),
(349, 11, 0, '21_20160602163812.jpg', '2016-06-02 19:38:12', '2016-06-02 19:38:12'),
(350, 11, 0, '18_20160602163813.jpg', '2016-06-02 19:38:13', '2016-06-02 19:38:13'),
(351, 20, 0, '03_20160602164507.jpg', '2016-06-02 19:45:11', '2016-06-02 19:45:11'),
(352, 20, 0, '06_20160602164509.jpg', '2016-06-02 19:45:14', '2016-06-02 19:45:14'),
(353, 20, 0, '01_20160602164515.JPG', '2016-06-02 19:45:28', '2016-06-02 19:45:28'),
(354, 20, 0, '07_20160602164514.JPG', '2016-06-02 19:45:28', '2016-06-02 19:45:28'),
(355, 20, 0, '05_20160602164514.JPG', '2016-06-02 19:45:28', '2016-06-02 19:45:28'),
(356, 20, 0, '02_20160602164514.JPG', '2016-06-02 19:45:30', '2016-06-02 19:45:30'),
(357, 20, 0, '08_20160602164517.JPG', '2016-06-02 19:45:30', '2016-06-02 19:45:30'),
(358, 20, 0, '09_20160602164519.JPG', '2016-06-02 19:45:35', '2016-06-02 19:45:35'),
(359, 20, 0, '10_20160602164535.JPG', '2016-06-02 19:45:41', '2016-06-02 19:45:41'),
(360, 20, 0, '12_20160602164536.JPG', '2016-06-02 19:45:46', '2016-06-02 19:45:46'),
(361, 20, 0, '15_20160602164542.JPG', '2016-06-02 19:45:47', '2016-06-02 19:45:47'),
(362, 20, 0, '13_20160602164540.JPG', '2016-06-02 19:45:47', '2016-06-02 19:45:47'),
(363, 20, 0, '11_20160602164535.JPG', '2016-06-02 19:45:48', '2016-06-02 19:45:48'),
(364, 20, 0, '14_20160602164539.JPG', '2016-06-02 19:45:50', '2016-06-02 19:45:50'),
(365, 20, 0, '17_20160602164550.JPG', '2016-06-02 19:46:00', '2016-06-02 19:46:00'),
(366, 20, 0, '16_20160602164546.JPG', '2016-06-02 19:46:00', '2016-06-02 19:46:00'),
(367, 20, 0, '18_20160602164554.JPG', '2016-06-02 19:46:02', '2016-06-02 19:46:02'),
(368, 20, 0, '20_20160602164555.JPG', '2016-06-02 19:46:07', '2016-06-02 19:46:07'),
(369, 20, 0, '19_20160602164555.JPG', '2016-06-02 19:46:08', '2016-06-02 19:46:08'),
(370, 27, 0, '04_20160602164754.jpg', '2016-06-02 19:47:55', '2016-06-02 19:47:55'),
(371, 27, 0, '06_20160602164754.jpg', '2016-06-02 19:47:56', '2016-06-02 19:47:56'),
(372, 27, 0, '05_20160602164754.jpg', '2016-06-02 19:47:56', '2016-06-02 19:47:56'),
(373, 27, 0, '02_20160602164754.jpg', '2016-06-02 19:47:56', '2016-06-02 19:47:56'),
(374, 27, 0, '01_20160602164754.jpg', '2016-06-02 19:47:56', '2016-06-02 19:47:56'),
(375, 27, 0, '03_20160602164754.jpg', '2016-06-02 19:47:56', '2016-06-02 19:47:56'),
(376, 27, 0, '09_20160602164757.jpg', '2016-06-02 19:47:57', '2016-06-02 19:47:57'),
(377, 27, 0, '07_20160602164757.jpg', '2016-06-02 19:47:57', '2016-06-02 19:47:57'),
(378, 27, 0, '10_20160602164757.jpg', '2016-06-02 19:47:58', '2016-06-02 19:47:58'),
(379, 27, 0, '08_20160602164757.jpg', '2016-06-02 19:47:58', '2016-06-02 19:47:58'),
(380, 27, 0, '12_20160602164758.jpg', '2016-06-02 19:47:58', '2016-06-02 19:47:58'),
(381, 27, 0, '11_20160602164758.jpg', '2016-06-02 19:47:58', '2016-06-02 19:47:58'),
(382, 27, 0, '14_20160602164759.jpg', '2016-06-02 19:48:00', '2016-06-02 19:48:00'),
(383, 27, 0, '13_20160602164759.jpg', '2016-06-02 19:48:00', '2016-06-02 19:48:00'),
(384, 27, 0, '15_20160602164759.jpg', '2016-06-02 19:48:00', '2016-06-02 19:48:00'),
(385, 4, 0, '03_20160602165239.JPG', '2016-06-02 19:52:44', '2016-06-02 19:52:44'),
(386, 4, 0, '05_20160602165240.JPG', '2016-06-02 19:52:47', '2016-06-02 19:52:47'),
(387, 4, 0, '02_20160602165243.JPG', '2016-06-02 19:52:49', '2016-06-02 19:52:49'),
(388, 4, 0, '06_20160602165243.JPG', '2016-06-02 19:52:54', '2016-06-02 19:52:54'),
(389, 4, 0, '04_20160602165245.JPG', '2016-06-02 19:52:55', '2016-06-02 19:52:55'),
(390, 4, 0, '01_20160602165243.JPG', '2016-06-02 19:52:56', '2016-06-02 19:52:56'),
(391, 4, 0, '07_20160602165305.JPG', '2016-06-02 19:53:16', '2016-06-02 19:53:16'),
(392, 4, 0, '11_20160602165312.JPG', '2016-06-02 19:53:18', '2016-06-02 19:53:18'),
(393, 4, 0, '08_20160602165319.JPG', '2016-06-02 19:53:29', '2016-06-02 19:53:29'),
(394, 4, 0, '09_20160602165323.JPG', '2016-06-02 19:53:32', '2016-06-02 19:53:32'),
(395, 4, 0, '10_20160602165321.JPG', '2016-06-02 19:53:32', '2016-06-02 19:53:32'),
(396, 4, 0, '12_20160602165324.JPG', '2016-06-02 19:53:37', '2016-06-02 19:53:37'),
(397, 4, 0, '14_20160602165343.JPG', '2016-06-02 19:53:48', '2016-06-02 19:53:48'),
(398, 4, 0, '18_20160602165353.JPG', '2016-06-02 19:54:02', '2016-06-02 19:54:02'),
(399, 4, 0, '13_20160602165344.JPG', '2016-06-02 19:54:04', '2016-06-02 19:54:04'),
(400, 4, 0, '16_20160602165352.JPG', '2016-06-02 19:54:06', '2016-06-02 19:54:06'),
(401, 4, 0, '17_20160602165352.JPG', '2016-06-02 19:54:08', '2016-06-02 19:54:08'),
(402, 4, 0, '20_20160602165400.JPG', '2016-06-02 19:54:15', '2016-06-02 19:54:15'),
(403, 4, 0, '19_20160602165356.JPG', '2016-06-02 19:54:20', '2016-06-02 19:54:20'),
(404, 4, 0, '15_20160602165441.JPG', '2016-06-02 19:55:01', '2016-06-02 19:55:01'),
(405, 6, 0, '3_20160602165621.JPG', '2016-06-02 19:56:25', '2016-06-02 19:56:25'),
(406, 6, 0, '5_20160602165621.JPG', '2016-06-02 19:56:26', '2016-06-02 19:56:26'),
(407, 6, 0, '1_20160602165622.JPG', '2016-06-02 19:56:29', '2016-06-02 19:56:29'),
(408, 6, 0, '4_20160602165622.jpg', '2016-06-02 19:56:29', '2016-06-02 19:56:29'),
(409, 6, 0, '2_20160602165623.JPG', '2016-06-02 19:56:31', '2016-06-02 19:56:31'),
(410, 5, 0, '01_20160602170218.JPG', '2016-06-02 20:02:32', '2016-06-02 20:02:32'),
(411, 5, 0, '02_20160602170220.JPG', '2016-06-02 20:02:33', '2016-06-02 20:02:33'),
(412, 5, 0, '03_20160602170216.JPG', '2016-06-02 20:02:35', '2016-06-02 20:02:35'),
(413, 5, 0, '06_20160602170220.JPG', '2016-06-02 20:02:37', '2016-06-02 20:02:37'),
(414, 5, 0, '05_20160602170218.JPG', '2016-06-02 20:02:38', '2016-06-02 20:02:38'),
(415, 5, 0, '04_20160602170223.JPG', '2016-06-02 20:02:46', '2016-06-02 20:02:46'),
(416, 5, 0, '07_20160602170240.JPG', '2016-06-02 20:03:02', '2016-06-02 20:03:02'),
(417, 5, 0, '08_20160602170248.JPG', '2016-06-02 20:03:06', '2016-06-02 20:03:06'),
(418, 5, 0, '11_20160602170254.JPG', '2016-06-02 20:03:09', '2016-06-02 20:03:09'),
(419, 5, 0, '10_20160602170254.JPG', '2016-06-02 20:03:10', '2016-06-02 20:03:10'),
(420, 5, 0, '13_20160602170257.JPG', '2016-06-02 20:03:11', '2016-06-02 20:03:11'),
(421, 5, 0, '09_20160602170250.JPG', '2016-06-02 20:03:13', '2016-06-02 20:03:13'),
(422, 5, 0, '14_20160602170320.JPG', '2016-06-02 20:03:30', '2016-06-02 20:03:30'),
(423, 5, 0, '18_20160602170338.jpg', '2016-06-02 20:03:45', '2016-06-02 20:03:45'),
(424, 5, 0, '15_20160602170336.JPG', '2016-06-02 20:03:50', '2016-06-02 20:03:50'),
(425, 5, 0, '17_20160602170339.JPG', '2016-06-02 20:03:54', '2016-06-02 20:03:54'),
(426, 5, 0, '16_20160602170338.JPG', '2016-06-02 20:04:00', '2016-06-02 20:04:00'),
(427, 28, 0, '01_20160602170530.jpg', '2016-06-02 20:05:30', '2016-06-02 20:05:30'),
(428, 28, 0, '02_20160602170530.jpg', '2016-06-02 20:05:31', '2016-06-02 20:05:31'),
(429, 28, 0, '03_20160602170530.jpg', '2016-06-02 20:05:31', '2016-06-02 20:05:31'),
(430, 28, 0, '04_20160602170531.jpg', '2016-06-02 20:05:31', '2016-06-02 20:05:31'),
(431, 28, 0, '05_20160602170531.jpg', '2016-06-02 20:05:31', '2016-06-02 20:05:31'),
(432, 28, 0, '07_20160602170531.jpg', '2016-06-02 20:05:32', '2016-06-02 20:05:32'),
(433, 28, 0, '06_20160602170531.jpg', '2016-06-02 20:05:32', '2016-06-02 20:05:32'),
(434, 28, 0, '09_20160602170532.jpg', '2016-06-02 20:05:34', '2016-06-02 20:05:34'),
(435, 28, 0, '08_20160602170532.jpg', '2016-06-02 20:05:34', '2016-06-02 20:05:34'),
(436, 28, 0, '11_20160602170533.jpg', '2016-06-02 20:05:34', '2016-06-02 20:05:34'),
(437, 28, 0, '10_20160602170534.jpg', '2016-06-02 20:05:34', '2016-06-02 20:05:34'),
(438, 29, 0, '01_20160602170752.jpg', '2016-06-02 20:07:53', '2016-06-02 20:07:53'),
(439, 29, 0, '02_20160602170753.jpg', '2016-06-02 20:07:53', '2016-06-02 20:07:53'),
(440, 29, 0, '03_20160602170753.jpg', '2016-06-02 20:07:54', '2016-06-02 20:07:54'),
(441, 29, 0, '04_20160602170754.jpg', '2016-06-02 20:07:54', '2016-06-02 20:07:54'),
(442, 29, 0, '06_20160602170754.jpg', '2016-06-02 20:07:55', '2016-06-02 20:07:55'),
(443, 29, 0, '05_20160602170754.jpg', '2016-06-02 20:07:56', '2016-06-02 20:07:56'),
(444, 29, 0, '11_20160602170756.jpg', '2016-06-02 20:07:56', '2016-06-02 20:07:56'),
(445, 29, 0, '10_20160602170755.jpg', '2016-06-02 20:07:56', '2016-06-02 20:07:56'),
(446, 29, 0, '09_20160602170756.jpg', '2016-06-02 20:07:57', '2016-06-02 20:07:57'),
(447, 29, 0, '12_20160602170757.jpg', '2016-06-02 20:07:57', '2016-06-02 20:07:57'),
(448, 29, 0, '15_20160602170758.jpg', '2016-06-02 20:07:58', '2016-06-02 20:07:58'),
(449, 29, 0, '14_20160602170759.jpg', '2016-06-02 20:07:59', '2016-06-02 20:07:59'),
(450, 29, 0, '18_20160602170759.jpg', '2016-06-02 20:08:00', '2016-06-02 20:08:00'),
(451, 29, 0, '16_20160602170759.jpg', '2016-06-02 20:08:00', '2016-06-02 20:08:00'),
(452, 29, 0, '13_20160602170759.jpg', '2016-06-02 20:08:01', '2016-06-02 20:08:01'),
(453, 29, 0, '17_20160602170800.jpg', '2016-06-02 20:08:01', '2016-06-02 20:08:01'),
(454, 29, 0, '20_20160602170802.jpg', '2016-06-02 20:08:02', '2016-06-02 20:08:02'),
(455, 29, 0, '19_20160602170801.jpg', '2016-06-02 20:08:03', '2016-06-02 20:08:03'),
(456, 29, 0, '21_20160602170803.jpg', '2016-06-02 20:08:04', '2016-06-02 20:08:04'),
(457, 30, 0, '03_20160602171009.jpg', '2016-06-02 20:10:10', '2016-06-02 20:10:10'),
(458, 30, 0, '04_20160602171010.jpg', '2016-06-02 20:10:11', '2016-06-02 20:10:11'),
(459, 30, 0, '02_20160602171010.jpg', '2016-06-02 20:10:11', '2016-06-02 20:10:11'),
(460, 30, 0, '01_20160602171010.jpg', '2016-06-02 20:10:11', '2016-06-02 20:10:11'),
(461, 30, 0, '05_20160602171011.jpg', '2016-06-02 20:10:12', '2016-06-02 20:10:12'),
(462, 30, 0, '06_20160602171011.jpg', '2016-06-02 20:10:12', '2016-06-02 20:10:12'),
(463, 30, 0, '07_20160602171014.jpg', '2016-06-02 20:10:15', '2016-06-02 20:10:15'),
(464, 30, 0, '10_20160602171015.jpg', '2016-06-02 20:10:15', '2016-06-02 20:10:15'),
(465, 30, 0, '09_20160602171015.jpg', '2016-06-02 20:10:15', '2016-06-02 20:10:15'),
(466, 30, 0, '11_20160602171015.jpg', '2016-06-02 20:10:15', '2016-06-02 20:10:15'),
(467, 30, 0, '08_20160602171015.jpg', '2016-06-02 20:10:16', '2016-06-02 20:10:16'),
(468, 30, 0, '12_20160602171015.jpg', '2016-06-02 20:10:16', '2016-06-02 20:10:16'),
(469, 30, 0, '13_20160602171016.jpg', '2016-06-02 20:10:17', '2016-06-02 20:10:17'),
(470, 8, 0, '04_20160602171740.JPG', '2016-06-02 20:17:53', '2016-06-02 20:17:53'),
(471, 8, 0, '06_20160602171743.JPG', '2016-06-02 20:18:05', '2016-06-02 20:18:05'),
(472, 8, 0, '01_20160602171741.JPG', '2016-06-02 20:18:21', '2016-06-02 20:18:21'),
(473, 8, 0, '02_20160602171745.JPG', '2016-06-02 20:18:17', '2016-06-02 20:18:17'),
(474, 8, 0, '07_20160602171757.JPG', '2016-06-02 20:18:25', '2016-06-02 20:18:25'),
(475, 8, 0, '05_20160602171752.jpg', '2016-06-02 20:18:25', '2016-06-02 20:18:25'),
(476, 8, 0, '03_20160602171741.JPG', '2016-06-02 20:18:25', '2016-06-02 20:18:25'),
(477, 8, 0, '08_20160602171813.JPG', '2016-06-02 20:18:31', '2016-06-02 20:18:31'),
(478, 8, 0, '09_20160602171826.JPG', '2016-06-02 20:18:37', '2016-06-02 20:18:37'),
(479, 8, 0, '11_20160602171835.JPG', '2016-06-02 20:18:51', '2016-06-02 20:18:51'),
(480, 8, 0, '14_20160602171842.JPG', '2016-06-02 20:18:58', '2016-06-02 20:18:58'),
(481, 8, 0, '10_20160602171842.jpg', '2016-06-02 20:18:59', '2016-06-02 20:18:59'),
(482, 8, 0, '12_20160602171837.JPG', '2016-06-02 20:19:05', '2016-06-02 20:19:05'),
(483, 8, 0, '13_20160602171843.jpg', '2016-06-02 20:19:15', '2016-06-02 20:19:15'),
(484, 14, 0, '1_20160614173339.jpg', '2016-06-14 20:33:40', '2016-06-14 20:33:40'),
(485, 14, 0, '2_20160614173339.jpg', '2016-06-14 20:33:40', '2016-06-14 20:33:40'),
(486, 14, 0, '5_20160614173339.jpg', '2016-06-14 20:33:40', '2016-06-14 20:33:40'),
(487, 14, 0, '4_20160614173339.jpg', '2016-06-14 20:33:40', '2016-06-14 20:33:40'),
(488, 14, 0, '3_20160614173339.jpg', '2016-06-14 20:33:40', '2016-06-14 20:33:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes`
--

CREATE TABLE `publicacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `publicacoes`
--

INSERT INTO `publicacoes` (`id`, `titulo`, `slug`, `data`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'Casa e Decoração', 'casa-e-decoracao', '2009-12', '1_20160530201026.jpg', '2016-05-25 19:55:54', '2016-05-30 23:10:27'),
(2, 'Projeto para Quartos', 'projeto-para-quartos', '2009-12', '1_20160530201659.jpg', '2016-05-30 23:17:02', '2016-05-30 23:17:02'),
(3, 'Arquitetura e Construção', 'arquitetura-e-construcao', '2010-04', '1_20160530201913.jpg', '2016-05-30 23:19:14', '2016-05-30 23:19:14'),
(4, 'Banheiros', 'banheiros', '2010-03', '1_20160530202049.jpg', '2016-05-30 23:20:50', '2016-05-30 23:20:50'),
(5, 'Casa e Construção', 'casa-e-construcao', '2010-08', '1_20160530202150.jpg', '2016-05-30 23:21:54', '2016-05-30 23:21:54'),
(6, 'Casa e Decoração', 'casa-e-decoracao-1', '2010-03', '1_20160530202416.jpg', '2016-05-30 23:24:19', '2016-05-30 23:24:19'),
(7, 'Kaza', 'kaza', '2010-11', '1_20160530202526.jpg', '2016-05-30 23:25:30', '2016-05-30 23:25:30'),
(8, 'Pequenos Ambientes', 'pequenos-ambientes', '2010-04', '1_20160530202626.jpg', '2016-05-30 23:26:31', '2016-05-30 23:26:31'),
(9, 'Quartos e Closets', 'quartos-e-closets', '2010-01', '2_20160530202751.jpg', '2016-05-30 23:27:54', '2016-05-30 23:27:54'),
(10, 'Casa e Decoração', 'casa-e-decoracao-2', '2011-08', '1_20160530202906.jpg', '2016-05-30 23:29:09', '2016-05-30 23:29:09'),
(11, 'Casa e Decoração', 'casa-e-decoracao-3', '2011-01', '1_20160530202959.jpg', '2016-05-30 23:30:00', '2016-05-30 23:30:00'),
(12, 'Casa e Ambiente Bebê', 'casa-e-ambiente-bebe', '2012-01', '2_20160530203237.jpg', '2016-05-30 23:31:45', '2016-05-30 23:32:39'),
(13, 'Vídeo Som', 'video-som', '2012-01', '1_20160530203326.jpg', '2016-05-30 23:33:28', '2016-05-30 23:33:28'),
(14, 'Casa e Decoração', 'casa-e-decoracao-4', '2013-01', '1_20160530203604.jpg', '2016-05-30 23:36:07', '2016-05-30 23:36:07'),
(15, 'Pisos e Revestimentos', 'pisos-e-revestimentos', '2013-03', '1_20160530203655.jpg', '2016-05-30 23:36:57', '2016-05-30 23:36:57'),
(16, 'Quartos e Closets', 'quartos-e-closets-1', '2013-03', '1_20160530203818.jpg', '2016-05-30 23:38:20', '2016-05-30 23:38:20'),
(17, 'Mostra Casa e Gourmet', 'mostra-casa-e-gourmet', '2008-01', '1_20160530203927.jpg', '2016-05-30 23:39:29', '2016-05-30 23:39:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes_imagens`
--

CREATE TABLE `publicacoes_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `publicacao_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `publicacoes_imagens`
--

INSERT INTO `publicacoes_imagens` (`id`, `publicacao_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(4, 1, 0, '3_20160530201054.jpg', '2016-05-30 23:11:04', '2016-05-30 23:11:04'),
(5, 1, 0, '2_20160530201055.jpg', '2016-05-30 23:11:04', '2016-05-30 23:11:04'),
(6, 2, 0, '2_20160530201732.jpg', '2016-05-30 23:17:36', '2016-05-30 23:17:36'),
(7, 3, 0, '2_20160530201928.jpg', '2016-05-30 23:19:31', '2016-05-30 23:19:31'),
(8, 4, 0, '2_20160530202111.jpg', '2016-05-30 23:21:14', '2016-05-30 23:21:14'),
(9, 4, 0, '3_20160530202111.jpg', '2016-05-30 23:21:15', '2016-05-30 23:21:15'),
(10, 5, 0, '2_20160530202246.jpg', '2016-05-30 23:22:49', '2016-05-30 23:22:49'),
(11, 5, 0, '4_20160530202246.jpg', '2016-05-30 23:22:54', '2016-05-30 23:22:54'),
(12, 5, 0, '3_20160530202247.jpg', '2016-05-30 23:22:56', '2016-05-30 23:22:56'),
(13, 6, 0, '2_20160530202449.jpg', '2016-05-30 23:25:00', '2016-05-30 23:25:00'),
(14, 6, 0, '4_20160530202452.jpg', '2016-05-30 23:25:02', '2016-05-30 23:25:02'),
(15, 6, 0, '3_20160530202451.jpg', '2016-05-30 23:25:02', '2016-05-30 23:25:02'),
(16, 6, 0, '5_20160530202451.jpg', '2016-05-30 23:25:02', '2016-05-30 23:25:02'),
(17, 7, 0, '2_20160530202552.jpg', '2016-05-30 23:26:00', '2016-05-30 23:26:00'),
(18, 7, 0, '3_20160530202552.jpg', '2016-05-30 23:26:01', '2016-05-30 23:26:01'),
(19, 8, 0, '3_20160530202658.jpg', '2016-05-30 23:27:03', '2016-05-30 23:27:03'),
(20, 8, 0, '2_20160530202658.jpg', '2016-05-30 23:27:06', '2016-05-30 23:27:06'),
(21, 8, 0, '4_20160530202701.jpg', '2016-05-30 23:27:12', '2016-05-30 23:27:12'),
(22, 9, 0, '2_20160530202817.jpg', '2016-05-30 23:28:27', '2016-05-30 23:28:27'),
(23, 10, 0, '3_20160530202922.JPG', '2016-05-30 23:29:27', '2016-05-30 23:29:27'),
(24, 10, 0, '2_20160530202925.jpg', '2016-05-30 23:29:33', '2016-05-30 23:29:33'),
(25, 11, 0, '2_20160530203014.jpg', '2016-05-30 23:30:19', '2016-05-30 23:30:19'),
(27, 12, 0, '3_20160530203158.jpg', '2016-05-30 23:32:03', '2016-05-30 23:32:03'),
(28, 12, 0, '1_20160530203217.JPG', '2016-05-30 23:32:21', '2016-05-30 23:32:21'),
(29, 13, 0, '2_20160530203346.jpg', '2016-05-30 23:33:52', '2016-05-30 23:33:52'),
(30, 13, 0, '4_20160530203347.jpg', '2016-05-30 23:33:54', '2016-05-30 23:33:54'),
(31, 13, 0, '3_20160530203351.JPG', '2016-05-30 23:33:58', '2016-05-30 23:33:58'),
(32, 14, 0, '2_20160530203623.JPG', '2016-05-30 23:36:27', '2016-05-30 23:36:27'),
(33, 14, 0, '3_20160530203622.JPG', '2016-05-30 23:36:27', '2016-05-30 23:36:27'),
(34, 15, 0, '2_20160530203710.jpg', '2016-05-30 23:37:18', '2016-05-30 23:37:18'),
(35, 16, 0, '2_20160530203840.JPG', '2016-05-30 23:38:43', '2016-05-30 23:38:43'),
(36, 17, 0, '3_20160530203949.jpg', '2016-05-30 23:39:50', '2016-05-30 23:39:50'),
(37, 17, 0, '2_20160530203950.JPG', '2016-05-30 23:39:53', '2016-05-30 23:39:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$VtEREXxEk4ZjfAk1zqpHVuT92.BEPflgdNuPB24ErnmlpUtZul5gm', 'yulUxYZV3tFHMkam6cozV2ObscrGhXurfYJT2MiILmgJInlE18mf9IHZ4ncw', NULL, '2016-05-30 22:04:09'),
(2, 'fernanda', 'contato@elianatoledo.com.br', '$2y$10$xvVI7lbnozwPT6c1f1MoVe7mFturqfFMBDfU4.BcHdbJX7mOi1.Xe', 'n5i8fyr7J7OSKQZxuEBTAguHdewyrLT1ssiEyQyiUer51hCVSO6ykAjYudKY', '2016-06-01 16:34:46', '2016-06-02 19:30:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aromas`
--
ALTER TABLE `aromas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisorias`
--
ALTER TABLE `divisorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisorias_abertura`
--
ALTER TABLE `divisorias_abertura`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiliario`
--
ALTER TABLE `mobiliario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiliario_abertura`
--
ALTER TABLE `mobiliario_abertura`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiliario_imagens`
--
ALTER TABLE `mobiliario_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobiliario_imagens_mobiliario_id_foreign` (`mobiliario_id`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`);

--
-- Indexes for table `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Indexes for table `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicacoes_imagens`
--
ALTER TABLE `publicacoes_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `publicacoes_imagens_publicacao_id_foreign` (`publicacao_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aromas`
--
ALTER TABLE `aromas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `divisorias`
--
ALTER TABLE `divisorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `divisorias_abertura`
--
ALTER TABLE `divisorias_abertura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mobiliario`
--
ALTER TABLE `mobiliario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `mobiliario_abertura`
--
ALTER TABLE `mobiliario_abertura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mobiliario_imagens`
--
ALTER TABLE `mobiliario_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=489;
--
-- AUTO_INCREMENT for table `publicacoes`
--
ALTER TABLE `publicacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `publicacoes_imagens`
--
ALTER TABLE `publicacoes_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `mobiliario_imagens`
--
ALTER TABLE `mobiliario_imagens`
  ADD CONSTRAINT `mobiliario_imagens_mobiliario_id_foreign` FOREIGN KEY (`mobiliario_id`) REFERENCES `mobiliario` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `projetos`
--
ALTER TABLE `projetos`
  ADD CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `publicacoes_imagens`
--
ALTER TABLE `publicacoes_imagens`
  ADD CONSTRAINT `publicacoes_imagens_publicacao_id_foreign` FOREIGN KEY (`publicacao_id`) REFERENCES `publicacoes` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
