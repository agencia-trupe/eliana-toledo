<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobiliarioImagem extends Model
{
    protected $table = 'mobiliario_imagens';

    protected $guarded = ['id'];

    public function scopeMobiliario($query, $id)
    {
        return $query->where('mobiliario_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
