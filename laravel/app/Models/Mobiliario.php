<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mobiliario extends Model
{
    protected $table = 'mobiliario';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\MobiliarioImagem', 'mobiliario_id')->ordenados();
    }
}
