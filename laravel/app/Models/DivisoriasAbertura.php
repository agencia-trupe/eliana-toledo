<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DivisoriasAbertura extends Model
{
    protected $table = 'divisorias_abertura';

    protected $guarded = ['id'];
}
