<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aromas extends Model
{
    protected $table = 'aromas';

    protected $guarded = ['id'];
}
