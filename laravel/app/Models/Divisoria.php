<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Divisoria extends Model
{
    protected $table = 'divisorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
