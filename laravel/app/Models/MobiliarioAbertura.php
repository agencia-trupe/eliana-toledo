<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobiliarioAbertura extends Model
{
    protected $table = 'mobiliario_abertura';

    protected $guarded = ['id'];
}
