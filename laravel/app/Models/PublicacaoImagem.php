<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacaoImagem extends Model
{
    protected $table = 'publicacoes_imagens';

    protected $guarded = ['id'];

    public function scopePublicacao($query, $id)
    {
        return $query->where('publicacao_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
