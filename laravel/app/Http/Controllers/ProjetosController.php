<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = ProjetoCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $projetos = $categoria->projetos()->get();

        return view('frontend.projetos.index', compact('categoria', 'projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        return view('frontend.projetos.show', compact('categoria', 'projeto'));
    }
}
