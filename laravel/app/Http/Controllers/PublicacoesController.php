<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Publicacao;

class PublicacoesController extends Controller
{
    public function index()
    {
        $publicacoes = Publicacao::ordenados()->get();

        return view('frontend.publicacoes.index', compact('publicacoes'));
    }

    public function show(Publicacao $publicacao)
    {
        return view('frontend.publicacoes.show', compact('publicacao'));
    }
}
