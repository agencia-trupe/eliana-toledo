<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Mobiliario;
use App\Models\MobiliarioAbertura;
use App\Models\Aromas;
use App\Models\Divisoria;
use App\Models\DivisoriasAbertura;

class ProdutosController extends Controller
{
    public function index($categoria = 'mobiliario')
    {
        switch ($categoria) {
            case 'mobiliario':
                return $this->mobiliario();
                break;
            case 'aromas':
                return $this->aromas();
                break;
            case 'divisorias':
                return $this->divisorias();
                break;
            default:
                return abort(404);
                break;
        }
    }

    public function mobiliario()
    {
        $abertura = MobiliarioAbertura::first();
        $mobiliario = Mobiliario::ordenados()->get();

        return view('frontend.produtos.mobiliario', compact('abertura', 'mobiliario'));
    }

    public function aromas()
    {
        $aromas = Aromas::first();

        return view('frontend.produtos.aromas', compact('aromas'));
    }

    public function divisorias()
    {
        $abertura = DivisoriasAbertura::first();
        $divisorias = Divisoria::ordenados()->get();

        return view('frontend.produtos.divisorias', compact('abertura', 'divisorias'));
    }
}
