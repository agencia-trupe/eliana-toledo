<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PublicacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Publicacao;
use App\Helpers\CropImage;

class PublicacoesController extends Controller
{
    private $image_config = [
        'width'  => 230,
        'height' => 300,
        'path'   => 'assets/img/publicacoes/capa/'
    ];

    public function index()
    {
        $registros = Publicacao::ordenados()->get();

        return view('painel.publicacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.publicacoes.create');
    }

    public function store(PublicacoesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Publicacao::create($input);
            return redirect()->route('painel.publicacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Publicacao $registro)
    {
        return view('painel.publicacoes.edit', compact('registro'));
    }

    public function update(PublicacoesRequest $request, Publicacao $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.publicacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Publicacao $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.publicacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
