<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DivisoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\Divisoria;
use App\Helpers\CropImage;

class DivisoriasController extends Controller
{
    private $image_config = [
        [
            'width'  => 230,
            'height' => 230,
            'path'   => 'assets/img/divisorias/thumbs/'
        ],
        [
            'width'  => 680,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/divisorias/'
        ]
    ];

    public function index()
    {
        $registros = Divisoria::ordenados()->get();

        return view('painel.divisorias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.divisorias.create');
    }

    public function store(DivisoriasRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Divisoria::create($input);

            $view = view('painel.divisorias.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function show(Divisoria $registro)
    {
        return $registro;
    }

    public function destroy(Divisoria $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.divisorias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
