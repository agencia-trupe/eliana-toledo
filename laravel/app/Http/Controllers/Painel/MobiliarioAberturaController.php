<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MobiliarioAberturaRequest;
use App\Http\Controllers\Controller;

use App\Models\MobiliarioAbertura;
use App\Helpers\CropImage;

class MobiliarioAberturaController extends Controller
{
    private $image_config = [
        'width'  => 230,
        'height' => null,
        'path'   => 'assets/img/mobiliario/abertura/'
    ];

    public function index()
    {
        $registro = MobiliarioAbertura::first();

        return view('painel.mobiliario.abertura.edit', compact('registro'));
    }

    public function update(MobiliarioAberturaRequest $request, MobiliarioAbertura $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);

            return redirect()->route('painel.mobiliario.abertura.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
