<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DivisoriasAberturaRequest;
use App\Http\Controllers\Controller;

use App\Models\DivisoriasAbertura;
use App\Helpers\CropImage;

class DivisoriasAberturaController extends Controller
{
    private $image_config = [
        'width'  => 230,
        'height' => null,
        'path'   => 'assets/img/divisorias/abertura/'
    ];

    public function index()
    {
        $registro = DivisoriasAbertura::first();

        return view('painel.divisorias.abertura.edit', compact('registro'));
    }

    public function update(DivisoriasAberturaRequest $request, DivisoriasAbertura $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);

            return redirect()->route('painel.divisorias.abertura.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
