<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PublicacoesImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Publicacao;
use App\Models\PublicacaoImagem;
use App\Helpers\CropImage;

class PublicacoesImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/publicacoes/imagens/thumbs/'
        ],
        [
            'width'   => 870,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/publicacoes/imagens/'
        ]
    ];

    public function index(Publicacao $publicacao)
    {
        $imagens = PublicacaoImagem::publicacao($publicacao->id)->ordenados()->get();

        return view('painel.publicacoes.imagens.index', compact('imagens', 'publicacao'));
    }

    public function show(Publicacao $publicacao, PublicacaoImagem $imagem)
    {
        return $imagem;
    }

    public function create(Publicacao $publicacao)
    {
        return view('painel.publicacoes.imagens.create', compact('publicacao'));
    }

    public function store(Publicacao $publicacao, PublicacoesImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $publicacao->imagens()->create($input);

            $view = view('painel.publicacoes.imagens.imagem', compact('publicacao', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Publicacao $publicacao, PublicacaoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.publicacoes.imagens.index', $publicacao)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Publicacao $publicacao)
    {
        try {

            $publicacao->imagens()->delete();
            return redirect()->route('painel.publicacoes.imagens.index', $publicacao)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
