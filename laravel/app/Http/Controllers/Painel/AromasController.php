<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AromasRequest;
use App\Http\Controllers\Controller;

use App\Models\Aromas;
use App\Helpers\CropImage;

class AromasController extends Controller
{
    private $image_config = [
        'width'  => 470,
        'height' => null,
        'path'   => 'assets/img/aromas/'
    ];

    public function index()
    {
        $registro = Aromas::first();

        return view('painel.aromas.edit', compact('registro'));
    }

    public function update(AromasRequest $request, Aromas $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);

            return redirect()->route('painel.aromas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
