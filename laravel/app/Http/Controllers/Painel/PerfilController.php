<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfilRequest;
use App\Http\Controllers\Controller;

use App\Models\Perfil;
use App\Helpers\CropImage;

class PerfilController extends Controller
{
    private $image_config = [
        'width'  => 360,
        'height' => null,
        'path'   => 'assets/img/perfil/'
    ];

    public function index()
    {
        $registro = Perfil::first();

        return view('painel.perfil.edit', compact('registro'));
    }

    public function update(PerfilRequest $request, Perfil $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);

            return redirect()->route('painel.perfil.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
