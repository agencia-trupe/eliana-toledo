<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MobiliarioImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Mobiliario;
use App\Models\MobiliarioImagem;
use App\Helpers\CropImage;

class MobiliarioImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/mobiliario/imagens/thumbs/'
        ],
        [
            'width'   => 680,
            'height'  => null,
            'path'    => 'assets/img/mobiliario/imagens/'
        ],
    ];

    public function index(Mobiliario $mobiliario)
    {
        $imagens = MobiliarioImagem::mobiliario($mobiliario->id)->ordenados()->get();

        return view('painel.mobiliario.imagens.index', compact('imagens', 'mobiliario'));
    }

    public function show(Mobiliario $mobiliario, MobiliarioImagem $imagem)
    {
        return $imagem;
    }

    public function create(Mobiliario $mobiliario)
    {
        return view('painel.mobiliario.imagens.create', compact('mobiliario'));
    }

    public function store(Mobiliario $mobiliario, MobiliarioImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['mobiliario_id'] = $mobiliario->id;

            $imagem = MobiliarioImagem::create($input);

            $view = view('painel.mobiliario.imagens.imagem', compact('mobiliario', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Mobiliario $mobiliario, MobiliarioImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.mobiliario.imagens.index', $mobiliario)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Mobiliario $mobiliario)
    {
        try {

            $mobiliario->imagens()->delete();
            return redirect()->route('painel.mobiliario.imagens.index', $mobiliario)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
