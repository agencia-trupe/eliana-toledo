<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MobiliarioRequest;
use App\Http\Controllers\Controller;

use App\Models\Mobiliario;
use App\Helpers\CropImage;

class MobiliarioController extends Controller
{
    private $image_config = [
        'width'  => 230,
        'height' => 230,
        'path'   => 'assets/img/mobiliario/thumbs/'
    ];

    public function index()
    {
        $registros = Mobiliario::ordenados()->get();

        return view('painel.mobiliario.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.mobiliario.create');
    }

    public function store(MobiliarioRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Mobiliario::create($input);
            return redirect()->route('painel.mobiliario.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);
        }
    }

    public function edit(Mobiliario $registro)
    {
        return view('painel.mobiliario.edit', compact('registro'));
    }

    public function update(MobiliarioRequest $request, Mobiliario $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.mobiliario.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function destroy(Mobiliario $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.mobiliario.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
