<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('projetos/{projetos_categoria?}', 'ProjetosController@index')->name('projetos');
    Route::get('projetos/{projetos_categoria}/{projeto}', 'ProjetosController@show')->name('projetos.show');
    Route::get('produtos/{produtos_categoria?}', 'ProdutosController@index')->name('produtos');
    Route::get('publicacoes', 'PublicacoesController@index')->name('publicacoes');
    Route::get('publicacoes/{publicacao}', 'PublicacoesController@show')->name('publicacoes.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@envio')->name('contato.envio');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('mobiliario/abertura', 'MobiliarioAberturaController', ['only' => ['index', 'update']]);
        Route::get('mobiliario/{mobiliario}/imagens/clear', [
            'as'   => 'painel.mobiliario.imagens.clear',
            'uses' => 'MobiliarioImagensController@clear'
        ]);
        Route::resource('mobiliario.imagens', 'MobiliarioImagensController');
        Route::resource('mobiliario', 'MobiliarioController');
		Route::resource('divisorias/abertura', 'DivisoriasAberturaController', ['only' => ['index', 'update']]);
		Route::resource('divisorias', 'DivisoriasController');
		Route::resource('aromas', 'AromasController', ['only' => ['index', 'update']]);
        Route::resource('projetos/categorias', 'ProjetosCategoriasController');
        Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');
		Route::resource('publicacoes', 'PublicacoesController');
        Route::get('publicacoes/{publicacoes}/imagens/clear', [
            'as'   => 'painel.publicacoes.imagens.clear',
            'uses' => 'PublicacoesImagensController@clear'
        ]);
        Route::resource('publicacoes.imagens', 'PublicacoesImagensController');
		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
