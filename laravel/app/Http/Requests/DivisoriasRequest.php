<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DivisoriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
        ];

        return $rules;
    }
}
