@extends('frontend.common.template')

@section('content')

    <div class="main publicacoes-show">
        <div class="center">
            @foreach($publicacao->imagens as $imagem)
            <img src="{{ asset('assets/img/publicacoes/imagens/'.$imagem->imagem) }}" alt="">
            @endforeach

            <a href="{{ route('publicacoes') }}">&laquo; voltar</a>
        </div>
    </div>

@endsection
