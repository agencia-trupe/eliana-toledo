@extends('frontend.common.template')

@section('content')

    <div class="main publicacoes-index">
        <div class="center">
            @foreach($publicacoes as $publicacao)
            <a href="{{ route('publicacoes.show', $publicacao->slug) }}">
                <div class="imagem">
                    <img src="{{ asset('assets/img/publicacoes/capa/'.$publicacao->imagem) }}" alt="">
                </div>
                <span>{!! Tools::formataData($publicacao->data) !!}</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
