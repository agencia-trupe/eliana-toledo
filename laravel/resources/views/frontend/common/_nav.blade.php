<a href="{{ route('perfil') }}" @if(Route::currentRouteName() === 'perfil') class="active" @endif>PERFIL</a>
<a href="{{ route('projetos') }}" @if(str_is('projetos*', Route::currentRouteName())) class="active" @endif">PROJETOS</a>
<a href="{{ route('produtos') }}" @if(Route::currentRouteName() === 'produtos') class="active" @endif">PRODUTOS</a>
<a href="{{ route('publicacoes') }}" @if(str_is('publicacoes*', Route::currentRouteName())) class="active" @endif">PUBLICAÇÕES</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif">CONTATO</a>
