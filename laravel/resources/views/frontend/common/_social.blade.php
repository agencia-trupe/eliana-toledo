<div class="social">
    @if($contato->facebook)
    <a href="{{ $contato->facebook }}" class="facebook">facebook</a>
    @endif
    @if($contato->instagram)
    <a href="{{ $contato->instagram }}" class="instagram">instagram</a>
    @endif
    @if($contato->pinterest)
    <a href="{{ $contato->pinterest }}" class="pinterest">pinterest</a>
    @endif
</div>
