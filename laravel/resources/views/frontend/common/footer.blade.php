    <footer @if(Route::currentRouteName() === 'home') class="home" @endif>
        <div class="center">
            <div class="info">
                <nav>
                    @include('frontend.common._nav')
                </nav>

                <span class="telefone">{{ $contato->telefone }}</span>

                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            </div>

            <p class="copyright">
                © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                <a href="http://trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
