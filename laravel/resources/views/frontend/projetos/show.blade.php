@extends('frontend.common.template')

@section('content')

    <div class="projetos-categorias">
        <div class="center">
            @foreach($categorias as $cat)
            <a href="{{ route('projetos', $cat->slug) }}" @if($cat->slug === $categoria->slug) class="active" @endif>{{ $cat->titulo }}</a>
            @endforeach
        </div>
    </div>

    <div class="main projetos-show">
        <div class="center">
            <div class="projeto-descricao">
                @if($projeto->planta)
                <a href="{{ asset('assets/img/projetos/planta/'.$projeto->planta) }}" class="planta fancybox">
                    <img src="{{ asset('assets/img/projetos/planta/thumbs/'.$projeto->planta) }}" alt="">
                </a>
                @endif

                <div class="texto @if($projeto->planta) texto-planta @endif">
                    <h1>{{ $projeto->titulo }}</h1>
                    <p>{{ $projeto->descricao }}</p>
                </div>
            </div>

            <div class="projeto-imagens">
                @foreach($projeto->imagens as $imagem)
                <a href="{{ asset('assets/img/projetos/imagens/ampliacao/'.$imagem->imagem) }}" class="fancybox" rel="projeto">
                    <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
