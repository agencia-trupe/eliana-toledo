@extends('frontend.common.template')

@section('content')

    <div class="projetos-categorias">
        <div class="center">
            @foreach($categorias as $cat)
            <a href="{{ route('projetos', $cat->slug) }}" @if($cat->slug === $categoria->slug) class="active" @endif>{{ $cat->titulo }}</a>
            @endforeach
        </div>
    </div>

    <div class="main projetos-index">
        <div class="center">
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
                <img src="{{ asset('assets/img/projetos/capa/'.$projeto->imagem) }}" alt="">
                <div class="overlay">
                    <div class="overlay-wrapper">
                        <span>{{ $projeto->titulo }}</span>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
