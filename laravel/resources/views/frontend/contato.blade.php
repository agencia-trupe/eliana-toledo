@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="imagem"><img src="{{ asset('assets/img/layout/contato.png') }}" alt=""></div>

            <form action="" id="form-contato" method="POST">
                <h2>FALE CONOSCO</h2>
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
                <div id="form-contato-response"></div>
            </form>

            <div class="info">
                <p class="telefone">{{ $contato->telefone }}</p>
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
                <div class="googlemaps">
                    {!! $contato->google_maps !!}
                </div>
            </div>
        </div>
    </div>

@endsection
