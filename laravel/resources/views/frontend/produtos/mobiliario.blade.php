@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="produtos-categorias">
                @include('frontend.produtos._categorias')
            </div>

            <div class="produtos-main produtos-mobiliario">
                <div class="abertura">
                    <img src="{{ asset('assets/img/mobiliario/abertura/'.$abertura->imagem) }}" alt="">
                    <div class="texto">
                        {!! $abertura->texto !!}
                    </div>
                </div>

                <div class="mobiliario-thumbs">
                    @foreach($mobiliario as $produto)
                    <a href="#" data-galeria="{{ $produto->id }}">
                        <img src="{{ asset('assets/img/mobiliario/thumbs/'.$produto->imagem) }}" alt="">
                        <div class="overlay">
                            <div class="overlay-wrapper">
                                <span>{{ $produto->titulo }}</span>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="hidden">
        @foreach($mobiliario as $projeto)
            @foreach($projeto->imagens as $imagem)
                <a href="{{ asset('assets/img/mobiliario/imagens/'.$imagem->imagem) }}" class="mobiliario-imagem" rel="galeria{{ $projeto->id }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
