<a href="{{ route('produtos', 'mobiliario') }}" @if(isset($mobiliario)) class="active" @endif>
    MOBILIÁRIO
</a>
<a href="{{ route('produtos', 'aromas') }}" @if(isset($aromas)) class="active" @endif>
    AROMAS
</a>
<a href="{{ route('produtos', 'divisorias') }}" @if(isset($divisorias)) class="active" @endif>
    DIVISÓRIAS
</a>
