@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="produtos-categorias">
                @include('frontend.produtos._categorias')
            </div>

            <div class="produtos-main produtos-aromas">
                <img src="{{ asset('assets/img/aromas/'.$aromas->imagem) }}" alt="">
                <div class="texto">
                    {!! $aromas->texto !!}
                </div>
            </div>
        </div>
    </div>

@endsection
