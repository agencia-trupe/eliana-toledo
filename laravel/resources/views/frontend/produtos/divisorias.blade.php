@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="produtos-categorias">
                @include('frontend.produtos._categorias')
            </div>

            <div class="produtos-main produtos-mobiliario">
                <div class="abertura">
                    <img src="{{ asset('assets/img/divisorias/abertura/'.$abertura->imagem) }}" alt="">
                    <div class="texto">
                        {!! $abertura->texto !!}
                    </div>
                </div>

                <div class="produtos-thumbs">
                    @foreach($divisorias as $produto)
                    <a href="{{ asset('assets/img/divisorias/'.$produto->imagem) }}" rel="produto">
                        <img src="{{ asset('assets/img/divisorias/thumbs/'.$produto->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
