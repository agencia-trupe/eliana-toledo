@extends('frontend.common.template')

@section('content')

    <div class="main not-found">
        <div class="center">
            <h1>Página não encontrada</h1>
        </div>
    </div>

@endsection
