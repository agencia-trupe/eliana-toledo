@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.divisorias.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Divisórias
    </a>

    <legend>
        <h2><small>Divisórias /</small> Abertura</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.divisorias.abertura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.divisorias.abertura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
