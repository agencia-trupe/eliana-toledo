@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mobiliário /</small> Editar Mobiliário</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mobiliario.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.mobiliario.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
