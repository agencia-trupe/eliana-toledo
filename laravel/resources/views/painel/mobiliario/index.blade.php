            {!! Form::open(['route' => ['painel.mobiliario.store'], 'files' => true, 'class' => 'clearfix pull-right']) !!}
                <div class="btn-group pull-right">
                    <a href="{{ route('painel.mobiliario.abertura.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Editar Abertura</a>
                    <span class="btn btn-success btn-sm" style="position:relative;overflow:hidden">
                        <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>
                        Adicionar Imagens
                        <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
                    </span>
                </div>
            {!! Form::close() !!}
        </h2>
    </legend>

@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Mobiliário
            <div class="btn-group pull-right">
                <a href="{{ route('painel.mobiliario.abertura.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Editar Abertura</a>
                <a href="{{ route('painel.mobiliario.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Mobiliário</a>
            </div>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="mobiliario">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Imagem de capa</th>
                <th>Imagens</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->titulo }}</td>
                <td><img src="{{ url('assets/img/mobiliario/thumbs/'.$registro->imagem) }}" alt="" style="width:100%;max-width:100px;height:auto;"></td>
                <td><a href="{{ route('painel.mobiliario.imagens.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.mobiliario.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.mobiliario.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
