@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mobiliário /</small> Adicionar Mobiliário</h2>
    </legend>

    {!! Form::open(['route' => 'painel.mobiliario.store', 'files' => true]) !!}

        @include('painel.mobiliario.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
