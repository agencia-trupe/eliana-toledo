@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.mobiliario.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Mobiliário
    </a>

    <legend>
        <h2><small>Mobiliário /</small> Abertura</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mobiliario.abertura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.mobiliario.abertura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
