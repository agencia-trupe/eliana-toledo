@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Publicações /</small> Adicionar Publicação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.publicacoes.store', 'files' => true]) !!}

        @include('painel.publicacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
