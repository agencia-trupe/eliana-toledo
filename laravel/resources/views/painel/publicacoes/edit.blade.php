@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Publicações /</small> Editar Publicação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.publicacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.publicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
