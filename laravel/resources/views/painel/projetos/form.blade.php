@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('projetos_categoria_id', 'Categoria') !!}
    {!! Form::select('projetos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem de capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/capa/'.$projeto->imagem) }}" style="display:block; margin-bottom: 10px; 100%; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('planta', 'Planta') !!}
@if($submitText == 'Alterar' && $projeto->planta)
    <img src="{{ url('assets/img/projetos/planta/thumbs/'.$projeto->planta) }}" style="display:block; margin-bottom: 10px; 100%; max-width: 100%;">
    <div class="alert alert-info">
        {{ Form::checkbox('remover_planta', 'true', null, ['id' => 'remover_planta']) }}
        {{ Form::label('remover_planta', 'Selecione para remover a imagem') }}
    </div>
@endif
    {!! Form::file('planta', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
