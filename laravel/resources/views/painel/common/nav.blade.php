<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.perfil*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.perfil.index') }}">Perfil</a>
    </li>
    <li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.projetos.index') }}">Projetos</a>
    </li>
    <li class="dropdown @if(str_is('painel.mobiliario*', Route::currentRouteName()) || str_is('painel.aromas*', Route::currentRouteName()) || str_is('painel.divisorias*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Produtos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('painel.mobiliario.index') }}">Mobiliário</a>
            </li>
        	<li>
        		<a href="{{ route('painel.aromas.index') }}">Aromas</a>
        	</li>
            <li>
                <a href="{{ route('painel.divisorias.index') }}">Divisórias</a>
            </li>
        </ul>
    </li>
	<li @if(str_is('painel.publicacoes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.publicacoes.index') }}">Publicações</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
