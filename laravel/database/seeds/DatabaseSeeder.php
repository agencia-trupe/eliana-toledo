<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(MobiliarioAberturaSeeder::class);
		$this->call(DivisoriasAberturaSeeder::class);
		$this->call(AromasSeeder::class);
		$this->call(PerfilSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
