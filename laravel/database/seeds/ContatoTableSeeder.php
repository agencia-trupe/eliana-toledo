<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'contato@elianatoledo.com.br',
            'telefone' => '11 2613·6484'
        ]);
    }
}
