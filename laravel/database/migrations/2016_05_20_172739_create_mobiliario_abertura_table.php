<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobiliarioAberturaTable extends Migration
{
    public function up()
    {
        Schema::create('mobiliario_abertura', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('mobiliario_abertura');
    }
}
