<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobiliarioTable extends Migration
{
    public function up()
    {
        Schema::create('mobiliario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('imagem');
            $table->text('descricao');
            $table->timestamps();
        });

        Schema::create('mobiliario_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mobiliario_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('mobiliario_id')->references('id')->on('mobiliario')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('mobiliario_imagens');
        Schema::drop('mobiliario');
    }
}
