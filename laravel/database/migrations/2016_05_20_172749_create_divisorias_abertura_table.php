<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisoriasAberturaTable extends Migration
{
    public function up()
    {
        Schema::create('divisorias_abertura', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('divisorias_abertura');
    }
}
