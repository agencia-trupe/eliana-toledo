<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacoesTable extends Migration
{
    public function up()
    {
        Schema::create('publicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('slug');
            $table->string('data');
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('publicacoes_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('publicacao_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('publicacao_id')->references('id')->on('publicacoes')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('publicacoes_imagens');
        Schema::drop('publicacoes');
    }
}
